// @ts-check
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2016-11-13T20:44:54+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/
'use strict';

const
  { defaultTo, isFunction, isNil, padEnd, toInteger } = require('lodash');

class BufferIn {
  /**
   * @param {Buffer} buffer
   */
  constructor(buffer) {
    this.fail = buffer ? false : true;
    this.buffer = buffer;
    this.idx = 0;
  }

  /**
   * @param {Buffer} buffer
   */
  setBuffer(buffer) {
    this.fail = buffer ? false : true;
    this.buffer = buffer;
    this.idx = 0;
  }

  /**
   * @param {number} size
   * @return {boolean}
   */
  _checkSize(size) {
    if (!this.fail && this.idx + size > this.buffer.length) {
      this.fail = true;
    }
    return !this.fail;
  }

  bytesLeft() {
    return (this.fail) ? 0 : (this.buffer.length - this.idx);
  }

  readUInt8() {
    if (!this._checkSize(1)) { return NaN; }
    return this.buffer.readUInt8(this.idx++);
  }

  readInt8() {
    if (!this._checkSize(1)) { return NaN; }
    return this.buffer.readInt8(this.idx++);
  }

  /**
   * @param  {boolean=} be
   */
  readUInt16(be) {
    if (!this._checkSize(2)) { return NaN; }
    const ret = be ?
      this.buffer.readUInt16BE(this.idx) :
      this.buffer.readUInt16LE(this.idx);
    this.idx += 2;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readInt16(be) {
    if (!this._checkSize(2)) { return NaN; }
    const ret = be ?
      this.buffer.readInt16BE(this.idx) :
      this.buffer.readInt16LE(this.idx);
    this.idx += 2;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readUInt32(be) {
    if (!this._checkSize(4)) { return NaN; }
    const ret = be ?
      this.buffer.readUInt32BE(this.idx) :
      this.buffer.readUInt32LE(this.idx);
    this.idx += 4;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readInt32(be) {
    if (!this._checkSize(4)) { return NaN; }
    const ret = be ?
      this.buffer.readInt32BE(this.idx) :
      this.buffer.readInt32LE(this.idx);
    this.idx += 4;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readUInt64(be) {
    if (!this._checkSize(8)) { return NaN; }
    // @ts-ignore: works with Buffer objects
    const ret = be ?
      this.buffer.readBigUInt64BE(this.idx) :
      this.buffer.readBigUInt64LE(this.idx);
    this.idx += 8;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readInt64(be) {
    if (!this._checkSize(8)) { return NaN; }
    // @ts-ignore: works with Buffer objects
    const ret = be ?
      this.buffer.readBigInt64BE(this.idx) :
      this.buffer.readBigInt64LE(this.idx);
    this.idx += 8;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readFloat(be) {
    if (!this._checkSize(4)) { return NaN; }
    const ret = be ?
      this.buffer.readFloatBE(this.idx) :
      this.buffer.readFloatLE(this.idx);
    this.idx += 4;
    return ret;
  }

  /**
   * @param  {boolean=} be
   */
  readDouble(be) {
    if (!this._checkSize(8)) { return NaN; }
    const ret = be ?
      this.buffer.readDoubleBE(this.idx) :
      this.buffer.readDoubleLE(this.idx);
    this.idx += 8;
    return ret;
  }

  /**
   * @param  {number} len
   */
  readString(len) {
    if (!this._checkSize(len)) { return ''; }
    const str = this.buffer.subarray(this.idx, this.idx + len).toString('utf8');
    this.idx += len;
    const idx = str.indexOf('\0');
    return (idx >= 0) ? str.substr(0, idx) : str;
  }

  /**
   * @param  {number} len
   */
  read(len) {
    if (!this._checkSize(len)) { return null; }

    const ret = this.buffer.subarray(this.idx, this.idx + len);
    this.idx += len;
    return ret;
  }

  /**
   * @param  {number} len
   */
  fwd(len) {
    if (!this._checkSize(len)) { return; }
    this.idx += len;
  }

  rewind() {
    this.setBuffer(this.buffer);
  }

  /**
   * @brief wrap a regular buffer in BufferIn object
   * @param  {BufferIn|Buffer} buffer
   * @return {BufferIn}
   */
  static wrap(buffer) {
    if (buffer instanceof BufferIn) {
      return buffer;
    }
    return new BufferIn(buffer);
  }
}

/**
 * @details endianness defaults to little-endian
 */
class BufferOut {
  /**
   * @param {Buffer} buffer
   */
  constructor(buffer) {
    this.fail = buffer ? false : true;
    this.buffer = buffer;
    this.idx = 0;
  }

  /**
   * @param {Buffer} buffer
   */
  setBuffer(buffer) {
    this.fail = buffer ? false : true;
    this.buffer = buffer;
    this.idx = 0;
  }

  bytesLeft() {
    return (this.fail) ? 0 : (this.buffer.length - this.idx);
  }

  /**
   * @param {number} num
   */
  writeUInt8(num) {
    if (!this._checkSize(1)) { return; }

    this.buffer.writeUInt8(num, this.idx++);
  }

  /**
   * @param {number} num
   */
  writeInt8(num) {
    if (!this._checkSize(1)) { return; }

    this.buffer.writeInt8(num, this.idx++);
  }

  /**
   * @param {number} num
   * @param {boolean=} be
   */
  writeUInt16(num, be) {
    if (!this._checkSize(2)) { return; }
    this.idx = be ?
      this.buffer.writeUInt16BE(num, this.idx) :
      this.buffer.writeUInt16LE(num, this.idx);
  }

  /**
   * @param {number} num
   * @param {boolean=} be
   */
  writeInt16(num, be) {
    if (!this._checkSize(2)) { return; }
    this.idx = be ?
      this.buffer.writeInt16BE(num, this.idx) :
      this.buffer.writeInt16LE(num, this.idx);
  }

  /**
   * @param {number} num
   * @param {boolean=} be
   */
  writeUInt32(num, be) {
    if (!this._checkSize(4)) { return; }
    this.idx = be ?
      this.buffer.writeUInt32BE(num, this.idx) :
      this.buffer.writeUInt32LE(num, this.idx);
  }

  /**
   * @param {number} num
   * @param {boolean=} be
   */
  writeInt32(num, be) {
    if (!this._checkSize(4)) { return; }
    this.idx = be ?
      this.buffer.writeInt32BE(num, this.idx) :
      this.buffer.writeInt32LE(num, this.idx);
  }

  /**
   * @param {number|bigint} num
   * @param {boolean=} be
   */
  writeUInt64(num, be) {
    if (!this._checkSize(8)) { return; }
    if (typeof num === 'number') { num = BigInt(toInteger(num)); }
    this.idx = be ?
      this.buffer.writeBigUInt64BE(num, this.idx) :
      this.buffer.writeBigUInt64LE(num, this.idx);
  }

  /**
   * @param {number|bigint} num
   * @param {boolean=} be
   */
  writeInt64(num, be) {
    if (!this._checkSize(8)) { return; }
    if (typeof num === 'number') { num = BigInt(toInteger(num)); }
    this.idx = be ?
      this.buffer.writeBigInt64BE(num, this.idx) :
      this.buffer.writeBigInt64LE(num, this.idx);
  }

  /**
   * @param {number} num
   * @param {boolean=} be
   */
  writeFloat(num, be) {
    if (!this._checkSize(4)) { return; }
    this.idx = be ?
      this.buffer.writeFloatBE(num, this.idx) :
      this.buffer.writeFloatLE(num, this.idx);
  }

  /**
   * @param {number} num
   * @param {boolean=} be
   */
  writeDouble(num, be) {
    if (!this._checkSize(8)) { return; }
    this.idx = be ?
      this.buffer.writeDoubleBE(num, this.idx) :
      this.buffer.writeDoubleLE(num, this.idx);
  }

  /**
   * @param {string} str
   * @param {number=} len
   */
  writeString(str, len) {
    if (isNil(str)) {
      str = '';
    }
    len = len || (str.length + 1);
    if (!this._checkSize(len)) {
      return;
    }
    if (len - 1 < str.length) {
      str = str.slice(0, len - 1);
    }
    str = padEnd(str, len, '\0');
    this.buffer.write(str, this.idx, len, 'utf8');
    this.idx += len;
  }

  /**
   * @param {string} string
   * @param {number} len
   * @param {string=} encoding
   */
  write(string, len, encoding) {
    len = defaultTo(len, string.length);
    len = Math.min(len, string.length);
    if (this._checkSize(len)) {
      /* @ts-ignore */
      this.buffer.write(string, this.idx, len, encoding);
      this.idx += len;
    }
  }

  /**
   * @param {Buffer|number[]} buffer
   */
  add(buffer) {
    if (this._checkSize(buffer.length)) {
      if (buffer instanceof Buffer) {
        /* @ts-ignore: checked with _checkSize */
        buffer.copy(this.buffer, this.idx, 0);
        this.idx += buffer.length;
      }
      else {
        for (let i = 0; i < buffer.length; ++i) {
          this.buffer.writeUInt8(buffer[i], this.idx++);
        }
      }
    }
  }

  /**
   * @param {number} sz
   * @return {boolean}
   */
  _checkSize(sz) {
    if (this.fail) {
      return false;
    }
    else if (this.idx + sz > this.buffer.length) {
      /* @ts-ignore: doesn't understand isFunction */
      if (isFunction(this.buffer.resize)) {
        /* @ts-ignore: doesn't understand isFunction */
        this.buffer.resize(this.buffer.length + sz);
      }
      if (this.idx + sz > this.buffer.length) {
        this.fail = true;
        return false;
      }
    }
    return true;
  }

  /**
   * @brief wrap a Buffer in a BufferOut object
   * @param  {Buffer|BufferOut} buffer
   * @return {BufferOut}
   */
  static wrap(buffer) {
    if (buffer instanceof BufferOut) {
      return buffer;
    }
    return new BufferOut(buffer);
  }
}
module.exports = { BufferIn, BufferOut };
