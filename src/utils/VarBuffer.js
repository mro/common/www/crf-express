// @ts-check
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2016-11-08T14:04:02+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/
'use strict';

const
  { isInteger, defaultTo } = require("lodash");

class VarBuffer {
  /**
   * @param {?(Buffer|number)=} buf
   */
  constructor(buf) {
    this.length = 0;
    if (isInteger(buf)) {
      /* @ts-ignore: checked above */
      this._buffer = Buffer.allocUnsafe(buf);
    }
    else if (buf instanceof Buffer) {
      this._buffer = buf;
      this.length = buf.length;
    }
    else {
      this._buffer = Buffer.allocUnsafe(VarBuffer.BUF_SZ);
    }
  }

  reset() {
    this.length = 0;
  }

  /**
   * @param {Buffer} data
   */
  add(data) {
    while (data.length > this._buffer.length - this.length) {
      this._enlarge();
    }
    data.copy(this._buffer, this.length, 0);
    this.length += data.length;
  }

  /** @return {Buffer} */
  buffer() {
    return this._buffer.slice(0, this.length);
  }

  /**
   * @param {number} start
   * @param {number} [end]
   * @return {Buffer}
   */
  slice(start, end) {
    return this._buffer.subarray(start, end ?? this.length);
  }

  /** @return {number} */
  reserved() {
    return this._buffer.length;
  }

  /** @param {number} size */
  resize(size) {
    while (size > this._buffer.length) {
      this._enlarge();
    }
    this.length = size;
  }

  /**
   * @brief remove the first elements of the buffer
   * @param {number} [n=1]
   */
  shift(n = 1) {
    if (this.length === 0 || !isInteger(n) || n < 1) { return; }
    if (n > this.length) { n = this.length; }
    const remainder = this.slice(n);
    this.reset();
    if (remainder.length > 0) { this.add(remainder); }
  }

  /**
   * @brief copy part of a buffer at a specific offset
   * @param  {Buffer|VarBuffer} target destination buffer
   * @param  {number} targetStart target start offset
   * @param  {number} sourceStart source (this) start offset
   * @param  {number} [sourceEnd] source end offset
   * @return {number} number of bytes copied
   */
  copy(target, targetStart, sourceStart, sourceEnd) {
    if (target instanceof VarBuffer) {
      target = target._buffer;
    }
    return this._buffer.copy(target, targetStart, sourceStart,
      defaultTo(sourceEnd, this.length));
  }

  _enlarge() {
    var old = this._buffer;
    this._buffer = Buffer.allocUnsafe(this._buffer.length * 2);
    old.copy(this._buffer, 0, 0, this.length);
  }
}

VarBuffer.BUF_SZ = 2048;

module.exports = VarBuffer;
