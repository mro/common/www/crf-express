// @ts-check

const
  { EventEmitter } = require('events'),
  debug = require('debug')('crf:socket'),
  { isNil, isEmpty } = require('lodash'),

  { CRFPacket } = require('./Packets/CRFPacket'),
  VarBuffer = require('./utils/VarBuffer'),
  TCPSocket = require("./TCPSocket");

let _id = 0;

/**
 * @typedef {CRFExpress.ServiceConfig} Config
 */

/**
 * @brief CRF Socket Client
 * @emits:
 *  - connected, when the connection succeeds
 *  - close, when the connection has been closed
 *  - packet, when a packet is received
 *  - error, when an error occurs
 */
class CRFSocket extends EventEmitter {

  /** @param {Config} [config] */
  constructor(config) {
    super();

    this._connected = false;
    /** @type {string|null} */
    this._host = null;
    /** @type {number|null} */
    this._port = null;
    /** @type {number} */
    this.id = _id++ % 256;
    /** @type {TCPSocket|null} */
    this._tcpSocket = null;
    /** @type {boolean} */
    this.idCheck = true; // enable writer id check
    /** @type {VarBuffer} */
    this._buffer = new VarBuffer();
    /** @type {NodeJS.Timeout|null} */
    this._buffTimer = null;
    /** @type {number} */
    this._payloadLen = 0; // used for packets reassembly
    /** @type {number} */
    this._bufCleanTimeout = // @ts-ignore: 'bufferCleanTimeout' is checked
      config?.bufferCleanTimeout > 0 ? config.bufferCleanTimeout * 1000 :
        CRFSocket.BUFFER_CLEAN_TIMEOUT;
  }

  /**
   * @param {Buffer} data
   * @returns {Array<CRFPacket|null>} // 'null' for invalid packets
   */
  _getPackets(data) {
    if (isEmpty(data)) { return []; }

    clearTimeout(this._buffTimer ?? undefined);
    this._buffTimer = null;

    this._buffer.add(data);

    const packets = [];
    while (
      this._buffer.length >= (CRFPacket.HEADER_LENGTH + this._payloadLen)) {

      if (this._payloadLen === 0) {
        // check for a CRF Packet deserializing the whole buffer
        const desPacket = CRFPacket.deserialize(this._buffer.buffer());
        this._payloadLen = desPacket?.getHeader()?.length ?? -1;
      }

      if (this._payloadLen >= 0) {
        const pcktLen = CRFPacket.HEADER_LENGTH + this._payloadLen;
        if (this._buffer.length < pcktLen) {
          debug("incomplete packet [socketId: %d]: received '%d' of '%d' bytes",
            this.id, this._buffer.length, pcktLen);
          break;
        }

        packets.push(CRFPacket.deserialize(Buffer.from(
          this._buffer.slice(0, pcktLen))));

        debug('packet extracted (#%d) [socketId: %d]', packets.length, this.id);
        this._buffer.shift(pcktLen);
      }
      else {
        const index = this._buffer.buffer().indexOf(CRFPacket.SYNC_PATTERN);

        this._buffer.shift(index === -1 ?
          // clean buffer retaining the last 'SYNC_PATTERN -1' bytes (worst case)
          this._buffer.length - (CRFPacket.SYNC_PATTERN.length - 1) :
          // clean buffer till the next SYNC_PATTERN
          index);

        packets.push(null);
        debug('data rejected [socketId: %d]: packet ill-formed', this.id);
      }
      this._payloadLen = 0;
    }

    // buffer cleaning
    if (this._buffer.length > 0) {
      debug('Buffer cointains residual data [socketId: %d]: clean in %d ms',
        this.id, this._bufCleanTimeout);
      this._buffTimer = setTimeout(() => {
        console.warn(
          `Buffer cleaned [socketId: ${this.id}]: no other data received ` +
          `before timeout expired (${this._bufCleanTimeout} ms)`);
        this._buffer.reset();
      }, this._bufCleanTimeout);
    }

    return packets;
  }

  updateId() {
    const newId = _id++ % 256;
    debug('updating socket id: %d -> %d', this.id, newId);
    this.id = newId;

    // update also TCP socket id
    if (this._tcpSocket) {
      this._tcpSocket.id =  this.id;
    }
  }

  /**
   * @param {string} host
   * @param {number} port
   * @returns {Promise<void>}
   */
  async connect(host, port) {
    if (this._connected) {
      debug("already connected to '%s:%d' [socketId: %d]",
        this._host, this._port, this.id);

      if (this._host !== host && this._port !== port) {
        throw new Error(`
          Attempt to connect to a different CRF endpoint (${host}:${port}) ` +
          `[socketId: ${this.id}]`);
      }
      return;
    }
    this._host = host;
    this._port = port;

    this._tcpSocket = new TCPSocket(this.id);

    this._tcpSocket
    .once('connected', () => {
      this._connected = true;
      this.emit('connected');
    })
    .on('error', (err) => {
      debug('error: %s [socketId: %d]', err.message, this.id);
      if (this._connected) { this.emit('error', err); }
    })
    .once('close', this.close.bind(this))
    .on('data', (data) => {
      debug('data received: [socketId: %d]', this.id);

      // packet splitting
      const packets = this._getPackets(data);
      packets.forEach((packet) => {
        if (isNil(packet)) {
          const err = new Error('Invalid packet. Packet discarded');
          debug('error: %s [socketId: %d]', err.message, this.id);
          this.emit('error', err);
          return;
        }

        const header = packet.getHeader();
        const payload = packet.getPayload();

        // checks (writerID + CRC)
        if (this.idCheck && header.writerID !== this.id) {
          const err = new Error('writerID mismatch. Closing socket...');
          debug('error: %s [socketId: %d]', err.message, this.id);
          this.emit('error', err);
          this.close();
          return;
        }

        const expectedCRC = CRFPacket.computeCRC(
          header.type, payload.length, header.timestamp, header.writerID);

        if (expectedCRC !== header.crc) {
          const err = new Error('CRC check failed. Packet discarded');
          debug('error: %s [socketId: %d]', err.message, this.id);
          this.emit('error', err);
          return;
        }

        this.emit('packet', packet);

        debug('packet emitted [socketId: %d]: %o', this.id, packet);
      });
    });

    await this._tcpSocket.connect(host, port);
  }

  /**
   * @param {CRFPacket} packet
   * @returns {boolean}
   */
  send(packet) {
    return this._tcpSocket?.send(packet.serialize()) ?? false;
  }

  close() {
    if (!this._connected) { return; }
    this._connected = false;

    this._tcpSocket?.removeAllListeners().close();
    this.emit('close');

    this._tcpSocket = null;
    this.idCheck = true;
  }
}
CRFSocket.BUFFER_CLEAN_TIMEOUT = 10000; // ms

module.exports = CRFSocket;
