// @ts-check

const
  { EventEmitter } = require('events'),
  { hrtime } = require('process'),
  debug = require('debug')('crf:service'),
  { makeDeferred, timeout } = require('@cern/prom'),
  { bindAll, isFinite, isString, some,
    toInteger, toLower } = require('lodash'),

  { CRFPacket, PacketType } = require('./Packets/CRFPacket'),
  SocketPool = require("./SocketPool");

/**
 * @typedef {import('./CRFSocket')} CRFSocket
 * @typedef {CRFExpress.ServiceConfig} Config
 * @typedef {{ socket: CRFSocket, host: string, port: number }} StreamConfig
 */

const TIME_SINCE_EPOCH_NS = BigInt(Date.now()) * BigInt(1e6);
const REL_START_TIME_NS = hrtime.bigint();

/** @return {bigint} */
function getDateNano() {
  return TIME_SINCE_EPOCH_NS + (hrtime.bigint() - REL_START_TIME_NS);
}

let _cmdId = 0;

class Service {
  /**
   * @param {string} host
   * @param {number} port
   * @param {string} name
   * @param {Config} [config]
   **/
  constructor(host, port, name, config) {
    /** @type {string} */
    this.host = isString(host) ? host : 'localhost';
    /** @type {number} */
    this.port = isFinite(port) ? port : Service.DEFAULT_PORT;
    /** @type {string} */
    this.name = isString(name) ? name : `${this.host}:${this.port}`;

    this._socketPool = new SocketPool(name, config);
  }

  destroy() {
    debug("destroying the service '%s'...", this.name);

    this._socketPool.destroy();

    debug("service '%s' destroyed", this.name);
  }

  /**
   * @brief get commmand reply from CRF endpoint (if any)
   * @param {any} command
   * @param {number} [ms=10000] - timeout
   * @returns {Promise<any>}
   */
  async getCmdReply(command, ms = 10000) {
    /** @type {CRFSocket|null} */
    let crfSocket;
    ms = ms >= 0 ? ms : 10000;

    const cmdIdCheck =
      !some([ 'false', '0' ], (v) => toLower(command?.cmdIdCheck) === v);
    const writerIdCheck =
      !some([ 'false', '0' ], (v) => toLower(command?.writerIdCheck) === v);

    let id;
    if (cmdIdCheck) {
      id = toInteger(command.id) ?? _cmdId++;
      command.id = id;
    }

    const start = Date.now();

    const cmdReplyDef = makeDeferred();

    const onPacket = (/** @type {CRFPacket} */pckt) => {
      if (!cmdReplyDef.isPending) { return; }

      const jsonReply = pckt.getJSON();
      if (cmdIdCheck && jsonReply?.id !== id) {
        cmdReplyDef.reject(new Error('Command id mismatch'));
      }
      else if (jsonReply?.replyCommand === 'error') {
        cmdReplyDef.reject(new Error(jsonReply?.message ?? 'Command failed'));
      }
      else {
        cmdReplyDef.resolve(pckt);
      }
    };
    const onError = (/** @type {Error} */err) => {
      if (cmdReplyDef.isPending) { cmdReplyDef.reject(err); }
    };

    cmdReplyDef.promise.catch((err) => console.error(
      `Failed to get the cmd reply [service: ${this.name}]: ${err.message} ` +
      `(code: ${err.code})`));

    try {
      crfSocket =
        await this._socketPool.acquireSocket(ms - (Date.now() - start));
      crfSocket.idCheck = writerIdCheck;

      crfSocket
      .on('packet', onPacket)
      .on('error', onError);

      await crfSocket.connect(this.host, this.port);

      const pckt =
        new CRFPacket(PacketType.JSON, writerIdCheck ? crfSocket.id : 0,
          command, getDateNano());

      if (!crfSocket.send(pckt)) {
        throw new Error('Failed to send the CRF packet');
      }

      const cmdReply = (await
      timeout(cmdReplyDef, ms - (Date.now() - start), 'CRF reply timeout'));

      // if the reply is not a JSON, send the whole packet
      return cmdReply.getJSON() ?? cmdReply.serialize();
    }
    catch (err) {
      onError(err);
      throw err;
    }
    finally {
      // @ts-ignore: socket acquisition can fail if timeout expires
      if (crfSocket) {
        crfSocket
        ?.removeListener('packet', onPacket)
        ?.removeListener('error', onError);

        this._socketPool.releaseSocket(crfSocket);
      }
    }
  }

  /**
   * @param {any} [config] - stream config
   * @param {number} [ms=10000] - timeout
   * @returns {Promise<CRFStream>}
   */
  async getStream(config, ms = 10000) {
    /** @type {CRFSocket} */
    let crfSocket;
    ms = ms >= 0 ? ms : 10000;

    const writerIdCheck =
      !some([ 'false', '0' ], (v) => toLower(config?.writerIdCheck) === v);

    try {
      crfSocket = await this._socketPool.acquireSocket(ms);
      crfSocket.idCheck = writerIdCheck;

      const stream = new CRFStream({
        socket: crfSocket,
        host: this.host,
        port: this.port
      });
      stream.once('close', () => this._socketPool.releaseSocket(crfSocket));

      await stream.open();

      return stream;
    }
    catch (err) {
      // @ts-ignore: socket acquisition can fail if timeout expires
      if (crfSocket) {
        this._socketPool.releaseSocket(crfSocket);
        crfSocket.close();
      }

      throw new Error(
        `Unable to establish a stream to '${this.name}': ${err.message}`);
    }
  }
}

/**
 * @brief streamer of CRF messages
 * @emits:
 *  - open, when the stream is open
 *  - close, when the stream has been closed
 *  - data, when a CRF JSON reply or CRF frame is received
 *  - error, when an error on the stream occurs
 */

class CRFStream extends EventEmitter {

  /**
   * @param {StreamConfig} config
   */
  constructor(config) {
    super();

    this._host = config?.host;
    this._port = config?.port;
    this._open = false;
    this._crfSocket = config?.socket;

    bindAll(this, [ 'onPacket', 'onError', 'close' ]);
  }

  /** @returns {Promise<void>} */
  async open() {
    if (this._open) {
      debug("stream to '%s:%d' already open [socketId: %d]",
        this._host, this._port, this._crfSocket.id);
      return;
    }

    if (!this._crfSocket) {
      throw new Error('Invalid CRF socket');
    }

    this._crfSocket
    .on('packet', this.onPacket)
    .on('error', this.onError)
    .once('close', this.close);

    await this._crfSocket.connect(this._host, this._port)
    .catch((err) => {
      console.error(`Failed to connect to '${this._host}:${this._port}' ` +
        `[socketId: ${this._crfSocket.id}]: ${err.message} (code: ${err.code}`);
      this._crfSocket.removeAllListeners();
      throw err;
    });

    this._open = true;

    this.emit('open');
    debug("stream to '%s:%d' open [socketId: %d]",
      this._host, this._port, this._crfSocket.id);
  }

  close() {
    if (!this._open) { return; }
    this._open = false;

    this._crfSocket.removeListener('packet', this.onPacket);
    this._crfSocket.removeListener('error', this.onError);
    this._crfSocket.removeListener('close', this.close);

    // NOTE: the socket has not to be closed here, it's not its responsability
    // The socket can be reused later

    this.emit('close');
    debug("stream to '%s:%d' closed [socketId: %d]",
      this._host, this._port, this._crfSocket.id);
  }

  /**
   * @param {any} command
   * @returns {boolean}
   */
  sendCmd(command) {
    if (!this._open) {
      debug('failed to send JSON command: stream not open');
      return false;
    }

    const pckt =
      new CRFPacket(PacketType.JSON,
        this._crfSocket?.idCheck ? this._crfSocket.id : 0,
        command, getDateNano());
    return this._crfSocket.send(pckt);
  }

  /**
   * @param {CRFPacket} packet
   */
  onPacket(packet) {
    const jsonReply = !!packet.getJSON();

    this.emit('data',
      jsonReply ? packet.getPayload().toString() : packet.serialize());

    debug("%s emitted [CRF stream '%s:%d', socketId: %d]",
      jsonReply ? 'JSON reply' : 'Packet',
      this._host, this._port, this._crfSocket.id);
  }

  /**
   * @param {Error} err
   */
  onError(err) {
    if (this._open) { this.emit('error', err); }
    debug("error: %s [CRF stream '%s:%d', socketId: %d]", err.message,
      this._host, this._port, this._crfSocket.id);
  }
}

Service.DEFAULT_PORT = 3000;

module.exports = Service;
