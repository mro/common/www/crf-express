// @ts-check

const
  debug = require('debug')('crf:packet'),
  { isBuffer, isEmpty, isFinite, isNil, isPlainObject, isString,
    toInteger, toString } = require('lodash'),

  { BufferIn, BufferOut } = require('../utils/IOBuffer');

/**
 * @typedef {{ syncPattern: string, type: number, length: number,
 *  timestamp: bigint, writerID: number, crc: number }} Header
 */

/** @enum {number} */
const PacketType = {
  FRAME: 119,
  RGBD_FRAME: 123,
  JSON: 801,
  NONE: 0
};

class CRFPacket {
  /**
   * @param {PacketType} type
   * @param {number} writerID
   * @param {any} payload
   * @param {number|bigint} timestamp
   */
  constructor(type, writerID, payload, timestamp) {
    /** @type {Buffer|null} */
    this._packetBuf = null;

    /** @type {PacketType} */
    this._type = type > 0 ? toInteger(type) : PacketType.NONE;

    /** @type {any} */
    this._json = null;

    if (isPlainObject(payload)) {
      this._json = payload;
      this._payload = Buffer.from(JSON.stringify(this._json));
    }
    else if (isString(payload)) {
      this._payload = Buffer.from(payload);
    }
    else if (isBuffer(payload)) {
      this._payload = payload;
    }
    else {
      this._payload = Buffer.from(toString(payload));
    }

    /** @type {number} */
    this._length = this._payload.length;
    /** @type {number} */
    this._writerID = writerID > 0 ? toInteger(writerID) : 0;

    /** @type {bigint} */
    this._timestamp = BigInt(0);
    if (timestamp > 0) {
      this._timestamp = isFinite(timestamp) ?
        BigInt(timestamp) : /** @type {bigint}*/(timestamp);
    }

    /** @type {number} */
    this._crc = CRFPacket.computeCRC(this._type, this._length,
      this._timestamp, this._writerID);
    /** @type {Header|null} */
    this._header = null;
  }

  /** @returns {Header} */
  getHeader() {
    if (isNil(this._header)) {
      this._header = {
        syncPattern: CRFPacket.SYNC_PATTERN,
        type: this._type,
        length: this._length,
        writerID: this._writerID,
        timestamp: this._timestamp,
        crc: this._crc
      };
    }
    return this._header;
  }

  /** @returns {any} */
  getJSON() {
    if (this._type !== PacketType.JSON) {
      debug('The packet does not contain a JSON (type: %d )', this._type);
      return null;
    }
    else if (this._json) {
      return this._json;
    }

    try {
      this._json = JSON.parse(this._payload);
      return this._json;
    }
    catch (err) {
      console.error('Failed to parse the CRF payload:', err.message);
      return null;
    }
  }

  /** @returns {Buffer} */
  getPayload() {
    return this._payload;
  }

  /** @returns {Buffer} */
  serialize() {
    if (isNil(this._packetBuf)) {
      const bufOut = new BufferOut(
        Buffer.allocUnsafe(CRFPacket.HEADER_LENGTH + this._length));

      // header
      bufOut.write(CRFPacket.SYNC_PATTERN, CRFPacket.SYNC_PATTERN.length);
      bufOut.writeUInt16(this._type);
      bufOut.writeUInt32(this._length);
      bufOut.writeUInt64(this._timestamp);
      bufOut.writeUInt8(this._writerID);
      bufOut.writeUInt8(this._crc);

      // payload
      bufOut.add(this._payload);

      // update the references
      this._packetBuf = bufOut.buffer;
      this._payload = this._packetBuf.subarray(CRFPacket.HEADER_LENGTH);
    }
    return this._packetBuf;
  }

  /**
   * @param {number} type
   * @param {number} length
   * @param {number|bigint} timestamp
   * @param {number} writerID
   * @returns {number}
   */
  static computeCRC(type, length, timestamp, writerID) {
    let tms = BigInt(0); /* jshint ignore:line, no 'new' prefix required*/
    if (timestamp > 0) {
      tms = isFinite(timestamp) ?
        BigInt(timestamp) :
        /** @type {bigint} */(timestamp);
    }

    return Number(
      (BigInt(toInteger(type) + toInteger(length) + toInteger(writerID))
        + tms) % BigInt(255));
  }

  /**
   * @param {Buffer} data
   * @returns {CRFPacket|null}
   */
  static deserialize(data) {
    if (isEmpty(data) || !isBuffer(data) ||
      !data.subarray(0, CRFPacket.SYNC_PATTERN.length)
      .equals(Buffer.from(CRFPacket.SYNC_PATTERN))) {
      debug('Unable to deserialize data: invalid input');
      return null;
    }

    const bufIn = new BufferIn(data);

    bufIn.fwd(CRFPacket.SYNC_PATTERN.length); // skip sync pattern

    const
      type = bufIn.readUInt16(),
      length = bufIn.readUInt32(),
      timestamp = bufIn.readUInt64(),
      writerID = bufIn.readUInt8(),
      crc = bufIn.readUInt8(),
      payload = bufIn.read(bufIn.bytesLeft());

    if (bufIn.fail) { return null; }

    const packet = new CRFPacket(type, writerID, payload, timestamp);
    // replace length and crc field with the ones read
    packet._length = length;
    packet._crc = crc;

    // optimize serialization preserving the original packet
    packet._packetBuf = data;

    return packet;
  }
}

CRFPacket.SYNC_PATTERN = '~~~~~~';
// bytes [ SYNC (6) + TYPE (2) + LENGTH (4) + TIMESTAMP (8) +
//         WRITERID (1) + CRC (1) ]
CRFPacket.HEADER_LENGTH = 22;
module.exports = { CRFPacket, PacketType };
