// @ts-check

const
  debug = require('debug')('crf:tcp:socket'),
  { makeDeferred } = require('@cern/prom'),
  { EventEmitter } = require("node:events"),
  { toString } = require('lodash'),
  { Socket } = require('net');

/**
 * @brief TCP Socket Client
 * @emits:
 *  - connected, when the connection succeeds
 *  - close, when the connection has been closed
 *  - data, when data are received
 *  - error, when an error occurs
 */

let socketId = 0;

class TCPSocket extends EventEmitter {

  /**
   * @param {number} [id]
   */
  constructor(id) {
    super();

    this._connecting = false;
    this._connected = false;

    /** @type {Socket|null} */
    this._socket = null;

    /** @type {string|null} */
    this._host = null;

    this._port = 0;

    this.id = id ?? socketId++;
  }

  /**
   * @param {string} host
   * @param {number} port
   * @returns {Promise<void>}
   */
  async connect(host, port) {
    if (this._connecting) {
      debug("already connected to '%s:%d' [socketId: %d]",
        this._host, this._port, this.id);

      if (this._host !== host && this._port !== port) {
        throw new Error(`
          Attempt to connect to a different host (${host}:${port}) ` +
          `[socketId: ${this.id}]`);
      }
      return;
    }
    else if (this._connected) {
      debug("already connected to '%s:%d' [socketId: %d]",
        this._host, this._port, this.id);
      return;
    }
    this._connecting = true;

    this._host = host ?? 'localhost';
    this._port = port;
    debug("connecting to '%s:%d' [socketId: %d]", this._host, this._port,
      this.id);

    const deferred = makeDeferred();

    this._socket = new Socket();

    this._socket
    .setKeepAlive(true, TCPSocket.KEEPALIVE_INTERVAL)
    .setTimeout(TCPSocket.CONN_TIMEOUT)
    .once('timeout', () => {
      debug('connection timeout (%s ms) [socketId: %d]',
        TCPSocket.CONN_TIMEOUT, this.id);
      this.close();
    })
    .on('error', (err) => {
      debug('error: %s [socketId: %d]', err.message, this.id);
      if (deferred.isPending) { deferred.reject(err); }
      else if (this._connected) { this.emit('error', err); }
    })
    .once('close', (hadError) => {
      this.close();
      debug('closed (hadError: %s) [socketId: %d]',
        hadError ? 'true' : 'false', this.id);
    })
    .on('data', this.onData.bind(this));

    this._socket.connect({ host, port }, () => {
      this._socket?.setTimeout(0);
      deferred.resolve(null);
      this._connecting = false;
      this._connected = true;
      this.emit('connected');
      debug("connected to '%s:%d' [socketId: %d]", this._host, this._port,
        this.id);
    });

    await deferred.promise;
  }

  /**
   * @param {string|Buffer} data
   */
  onData(data) {
    debug('received: %s [socketId: %d]',
      toString(data).substring(0, 50) + '...', this.id);
    this.emit('data', data);
    debug('data emitted [socketId: %d]', this.id);
  }

  /**
   *
   * @param {string|Buffer|Uint8Array} data
   * @returns {boolean}
   */
  send(data) {
    if (!this._connected) {
      console.warn(
        `Unable to send data [socketId: ${this.id}]: socket disconnected`);
      return false;
    }
    else if (!data) {
      console.warn(`Unable to send data [socketId: ${this.id}]: no data`);
      return false;
    }
    else if (!this._socket?.writable) {
      console.error(`Unable to send data [socketId: ${this.id}]`);
      return false;
    }
    debug('sending: %s [socketId: %d]',
      toString(data).substring(0, 50) + '...', this.id);

    this._socket.write(data);
    debug('data sent [socketId: %d]', this.id);
    return true;
  }

  close() {
    if (!this._connected) { return; }
    this._connected = false;
    this._connecting = false;

    this._socket?.removeAllListeners();
    if (!this._socket?.writableEnded) { this._socket?.end(); }
    if (!this._socket?.destroyed) { this._socket?.destroy(); }

    this._socket = null;

    this.emit('close');
  }
}

TCPSocket.CONN_TIMEOUT = 5000;
TCPSocket.KEEPALIVE_INTERVAL = 15000;

module.exports = TCPSocket;
