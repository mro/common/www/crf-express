import { EventEmitter } from 'events';
import { IRouter } from 'express';
import { OAS3Definition } from 'swagger-jsdoc';

export = CRFExpress;
export as namespace CRFExpress;

declare namespace CRFExpress {
  interface SwaggerConfig {
    definition?: Partial<OAS3Definition>
    basePath?: string
  }

  interface Config {
    swaggerConfig?: SwaggerConfig
  }

  interface ServiceConfig {
    socketNumber?: number,
    bufferCleanTimeout?: number
  }

  class ServicesManager {
    constructor(config?: Config)

    register(router: IRouter): void

    release(): void

    hasService(name: string): boolean

    getService(name: string, create?: boolean,
      config?: ServiceConfig): Service|null

    addAlias(host: string, name: string): boolean
  }

  class Service {
    constructor(
      host: string, port: number,
      name: string,
      config?: ServiceConfig)

    destroy(): void

    getCmdReply(command: any, ms?: number): Promise<any>

    getStream(config: any, ms?: number): Promise<CRFStream|null>
  }

  class CRFStream extends EventEmitter {
    constructor(socket: CRFSocket, host: string, port: number)

    open(): Promise<boolean>

    close(): void

    sendCmd(command: string, message: any): boolean
  }

  class TCPSocket extends EventEmitter {
    id: number

    constructor(id?: number)

    connect(host: string, port: number): Promise<boolean>

    send(data: string|Buffer|Uint8Array): boolean

    close(): void
  }

  class CRFSocket {
    constructor(config?: ServiceConfig)

    connect(host: string, port: number): Promise<void>

    send(data: string|Buffer, type: number): boolean

    close(): void
  }

  class SocketPool {
    serviceName: string

    constructor(
      serviceName: string,
      socketConfig?: ServiceConfig)

    acquireSocket(ms?: number): Promise<TCPSocket>

    releaseSocket(socket: TCPSocket): void

    destroy(): void
  }

  class CRFPacket {
    constructor(
      type: number,
      writerID: number,
      payload: string|Buffer,
      timestamp: number|bigint)

    getPayload(): Buffer|null

    getJSON(): string|null

    serialize(): Buffer

    static deserialize(data: Buffer): CRFPacket|null

    static computeCRC(type: number, length: number,
      timestamp: number|bigint, writerID: number): number
  }

  namespace CRFPacketType {
    const FRAME: number;
    const RGBD_FRAME: number;
    const JSON: number;
    const NONE: number;
  }

  class CRFMockServer {
    constructor(name: string, port?: number): void

    destroy(): void
  }
  namespace Mocks {
    class BaseDevice {
      constructor(name: string)

      register(server: CRFMockServer): void

      unregister(): void

      addHandler(name: string, handler: (args: any[]) => any): boolean

      removeHandler(name: string): boolean

      removeAllHandlers(): void

      sendJson(cmd: string, message: any, extra: any): void

      sendFrame(data: Buffer): void
    }

    class MissionManager extends BaseDevice {
      streamMode: number

      streamFrequency: number

      constructor(name: string)

      destroy(): void

      start(): void

      next(): void

      stop(): void

      pause(): void

      resume(): void

      goHome(): void

      recharge(): void

      getStatus(): void

      setStatus(status: any): void

      emergency(): void

      rearm(): void
    }

    namespace MissionManager {
      const STREAM_MODE: {
        UNKNOWN: number,
        FREQUENCY: number, // send status periodically
        DIFFERENTIAL: number // send status upon updates
      };
    }
  }
}
