// @ts-check
const
  debug = require('debug')('server'),
  express = require('express'),
  { attempt, noop } = require('lodash'),
  { makeDeferred } = require('@cern/prom'),
  ews = require('express-ws'),

  ServicesManager = require('./ServicesManager');

/**
 * @typedef {import('net').AddressInfo} AddressInfo
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 *
 * @typedef {{ port: number, basePath?: string }} Config
 */

class Server {
  /**
   * @param {Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this.servicesManager = null;
    this._prom = this.prepare(config);
  }

  /**
   * @param {Config} config
   */
  async prepare(config) {
    ews(this.app);

    this.router = express.Router();

    this.servicesManager = new ServicesManager({
      swaggerConfig: { basePath: config?.basePath }
    });
    this.servicesManager.register(this.router);

    this.app.use(config.basePath ?? '', this.router);
  }

  close() {
    this.servicesManager?.release();
    this.servicesManager = null;

    if (this.server) {
      this.server.close();
      this.server = null;
      debug('closed');
    }
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    debug('starting...');
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      debug('started');
      return attempt(cb || noop);
    });
    return def.promise;
  }

  /** @returns {AddressInfo | string | null} */
  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = Server;
