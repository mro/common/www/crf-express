// @ts-check

const
  { defaultsDeep, merge, omit } = require('lodash'),
  path = require('path'),
  swaggerJsdoc = require('swagger-jsdoc'),
  swaggerUi = require('swagger-ui-express'),

  packageJson = require('../../package.json');

/**
 * @typedef {import('express').Router} Router
 * @typedef {import('swagger-jsdoc').SwaggerDefinition} SwaggerDefinition
 * @typedef {import('swagger-jsdoc').OAS3Definition} OAS3Definition
 * @typedef {Omit<OAS3Definition, "openapi" | "info" | "server">} ExtraDoc
 */

class Swagger {

  /** @type {object} */
  #doc;

  /** @type {string} */
  #basePath;

  /**
   * @param {CRFExpress.SwaggerConfig} [config]
   */
  constructor(config) {
    const servers = [];
    this.#basePath = config?.basePath ?? '';
    if (this.#basePath) {
      servers.push({ url: this.#basePath, description: 'CRF Proxy base URL' });
    }

    /** @type {SwaggerDefinition} */
    const definition = defaultsDeep(config?.definition, {
      openapi: '3.0.0',
      info: {
        title: packageJson.name,
        version: '1.0.0', // Note: this is the API version
        description: packageJson?.description,
        contact: packageJson?.author
      },
      servers
    });

    try {
      /** @type {object} */
      this.#doc = swaggerJsdoc({
        failOnErrors: true,
        swaggerDefinition: definition,
        apis: [ path.join(__dirname, '../**/**.js') ]
      });
    }
    catch (err) {
      console.error('Invalid Swagger definition:', err.message);
    }
  }


  /**
   * @brief add documentation dynamically
   * @param {ExtraDoc} doc
   */
  addExtraDoc(doc) {
    merge(this.#doc, omit(doc, [ 'openapi', 'info', 'servers' ]));
  }

  /**
   * @param {Router} router
   */
  register(router) {
    const apiDefPath = '/api-docs.json';
    router.get(apiDefPath, (req, res) => res.json(this.#doc));

    const opts = {
      swaggerOptions: {
        displayRequestDuration: true,
        docExpansion: 'none',
        url: this.#basePath + apiDefPath
      }
    };

    router.use('/api-docs',
      swaggerUi.serveFiles(undefined, opts),
      swaggerUi.setup(undefined, opts));
  }

  release() {
    this.#doc = {};
  }
}

module.exports = Swagger;
