/**
 * @openapi
 * components:
 *  # ============ Schemas ============
 *  schemas:
 *    # ++++++++++ ServiceConfig ++++++++++
 *    ServiceConfig:
 *      type: object
 *      properties:
 *        servers:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              url:
 *                description: >
 *                  Service host.<br>
 *                  NOTE: the fomart MUST be either `<hostname>:<port>`
 *                  or `<ip:port>`.
 *                type: string
 *              name:
 *                description: >
 *                  Service name used as alias of the service host.
 *                type: string
 *            required: [ url, name ]
 *          example:
 *            - url: '192.168.1.3:1234'
 *              name: service1
 *            - url: 'crf-service.cern.ch:4567'
 *              name: service2
 *
 *    # ++++++++++ ServicePaths ++++++++++
 *    ServicePaths:
 *      type: object
 *      properties:
 *        paths:
 *          type: array
 *          items:
 *            type: string
 *
 *    # ++++++++++ JsonCommand ++++++++++
 *    JsonCommand:
 *      type: object
 *      properties:
 *        id:
 *          oneOf:
 *            - type: string
 *            - type: integer
 *        command:
 *          type: string
 *        message:
 *          type: object
 *      required: [ command ]
 *
 *    # ++++++++++ JsonReply ++++++++++
 *    JsonReply:
 *      type: object
 *      properties:
 *        id:
 *          oneOf:
 *            - type: string
 *            - type: integer
 *        command:
 *          type: string
 *          enum: [ 'reply' ]
 *        replyCommand:
 *          type: string
 *        message:
 *          type: object
 *      required: [ command, replyCommand, message ]
 *
 *    # ++++++++++ CRFPacket ++++++++++
 *    CRFPacket:
 *      type: string
 *      format: binary
 *
 *  # ============ Parameters ============
 *  parameters:
 *    # ++++++++++ Host ++++++++++
 *    Host:
 *      name: host
 *      in: path
 *      description: CRF host or its alias.
 *      required: true
 *      schema:
 *        type: string
 *      examples:
 *        'hostname + port':
 *          value: 'crf-service.cern.ch:4567'
 *        'ip + port':
 *          value: '192.168.1.1:1234'
 *
 *    # ++++++++++ Command ++++++++++
 *    Command:
 *      name: command
 *      in: path
 *      description: CRF command name.
 *      required: true
 *      schema:
 *        type: string
 *      example:
 *        getStatus
 *
 *    # ++++++++++ QueryWriterIdCheck ++++++++++
 *    QueryWriterIdCheck:
 *      name: writerIdCheck
 *      in: query
 *      description: >
 *        enable/disable the _writerId_ check in the
 *        contained in the CRF packet header
 *        (enable by default).
 *      schema:
 *        type: string
 *        enum: [ 'true', 'false', '1', '0' ]
 *
 *    # ++++++++++ QueryCommandIdCheck ++++++++++
 *    QueryCommandIdCheck:
 *      name: cmdIdCheck
 *      in: query
 *      description: >
 *        enable/disable the _id_ check in the
 *        contained in the CRF JSON command
 *        (enable by default).
 *      schema:
 *        type: string
 *        enum: [ 'true', 'false', '1', '0' ]
 *
 *    # ++++++++++ QueryStream ++++++++++
 *    QueryStream:
 *      name: stream
 *      in: query
 *      description: >
 *        enable streaming over HTTP if `true` or `1`
 *        (disabled by default)
 *      schema:
 *        type: string
 *        enum: [ 'true', 'false', '1', '0' ]
 *
 *    # ++++++++++ QueryTimeout ++++++++++
 *    QueryTimeout:
 *      name: timeout
 *      in: query
 *      description: >
 *        set a specific response timeout in seconds
 *        (10 seconds by default).
 *      schema:
 *        type: integer
 *
 *    # ++++++++++ QueryMessage ++++++++++
 *    QueryMessage:
 *      name: message
 *      in: query
 *      description: >
 *        string representing the JSON command message.
 *      schema:
 *        type: string
 *      example:
 *        %7B%20param1%3A%20123%2C%20param2%3A%20%27foo%27%20%7D
 *
 *  # ============ Responses ============
 *  responses:
 *    # ++++++++++ StringError ++++++++++
 *    StringError:
 *      description: Error occurred.
 *      content:
 *        text/html:
 *          schema:
 *            type: string
 *
 *    # ++++++++++ CRFCommandReply ++++++++++
 *    CRFCommandReply:
 *      description: CRF JSON reply or CRF Packet.
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/JsonReply'
 *        application/octet-stream:
 *          schema:
 *            $ref: '#/components/schemas/CRFPacket'
 */
