// @ts-check
/* eslint-disable max-lines */

const
  debug = require('debug')('crf:service:manager'),
  { assign, forEach, isBuffer, isString, lowerCase,
    size, some, split, toInteger, toLower, isEmpty,
    set, cloneDeep } = require('lodash'),
  bodyParser = require('body-parser'),

  Swagger = require('./Swagger/Swagger'),
  Service = require('./Service');

/**
 * @typedef {import('ws').WebSocket} WebSocket
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 * @typedef {CRFExpress.ServiceConfig} ServiceConfig
 * @typedef {import('swagger-jsdoc').OAS3Definition} OAS3Definition

 */

const WS_GOING_AWAY = 1001;
const WS_INTERNAL_ERROR = 1011;

/**
 * @param {any} command
 * @return {any} same command object with the eventual parsed 'message'
 * @note can alter the command object in input
 */
function parseCommandMessage(command) {
  const message = command?.message;
  if (isString(message) && message.startsWith('{')) {
    try {
      command['message'] = JSON.parse(message);
    }
    catch (err) {
      console.warn(`Failed to parse command message: ${err.message}`);
    }
  }
  return command;
}

/**
 * @param {string} host
 * @return {object}
 */
function httpAPI(host) {
  const get = {
    tags: [ 'control' ],
    summary: `send CRF JSON command to '${host}'`
  };

  get.parameters = [
    { "$ref": '#/components/parameters/Command' },
    { "$ref": '#/components/parameters/QueryWriterIdCheck' },
    { "$ref": '#/components/parameters/QueryCommandIdCheck' },
    { "$ref": '#/components/parameters/QueryStream' },
    { "$ref": '#/components/parameters/QueryTimeout' },
    { "$ref": '#/components/parameters/QueryMessage' }
  ];
  get.responses = {
    200: { "$ref": '#/components/responses/CRFCommandReply' },
    500: { "$ref": '#/components/responses/StringError' }
  };
  get.externalDocs = {
    description: 'CERN Robotic Framework Protocol',
    url:
      'https://mro-dev.web.cern.ch/docs/std/' +
      'cern-robotic-framework-protocol.html'
  };

  const put = cloneDeep(get);
  put.requestBody = { content: {
    "application/json": {
      schema: { "$ref": '#/components/schemas/JsonCommand' }
    }
  } };

  return { get, post: put, put };
}

/**
 * @param {string} host
 * @return {object}
 */
function wsAPI(host) {
  const get = {
    tags: [ 'control', 'websocket' ],
    summary: `open a WebSocket to communicate with '${host}'`,
    description:
      'Open a WebSocket to send multiple CRF JSON commands and receive ' +
      'CRF Replies or CRF packets (if they contain frames).\n\n' +
      'Once opened commands can easily be sent as shown in the examples ' +
      'below:\n\n' +
      '`ws.send(JSON.stringify({ command: "getStatus" }));`\n\n' +
      '`ws.send(JSON.stringify({ command: "setStatus", message: { ... } }))`'
  };

  get.parameters = [
    { "$ref": '#/components/parameters/QueryWriterIdCheck' },
    { "$ref": '#/components/parameters/QueryCommandIdCheck' }
  ];
  get.responses = {
    101: { description: 'Open a WebSocket.' },
    500: { "$ref": '#/components/responses/StringError' }
  };
  get.externalDocs = {
    description: 'CERN Robotic Framework Protocol',
    url:
      'https://mro-dev.web.cern.ch/docs/std/' +
      'cern-robotic-framework-protocol.html'
  };

  return { get };
}

class ServicesManager {

  /**
   * @param {CRFExpress.Config} [config]
   */
  constructor(config) {
    /** @type {Map<string, Service>} */
    this._serviceMap = new Map();

    /** @type {Map<string, string>} */
    this._aliasMap = new Map();

    /** @type {IRouter|null} */
    this._router = null;

    /** @type {CRFExpress.SwaggerConfig|undefined} */
    this._swaggerConfig = config?.swaggerConfig;

    /** @type {Swagger|null} */
    this._swagger = null;
  }

  /**
   * @param {IRouter} router
   */
  register(router) {
    if (!router) { return; }

    this.release();

    router
    .use(bodyParser.urlencoded({ extended: false }))
    .use(bodyParser.json());

    this._router = router;

    // Swagger
    this._swagger = new Swagger(this._swaggerConfig);
    this._swagger.register(router);

    /**
     * @openapi
     * /:
     *  post:
     *    summary: add new CRF service(s)
     *    tags: [ 'config' ]
     *    description: >
     *      Add new services by sending in the body an OpenRPC document
     *      containing at least the **servers** field.
     *      <br><br>
     *      **Note:** the _url_ MUST have the format `<hostname>:<port>` or
     *      `<ip>:<port>`.
     *    requestBody:
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/components/schemas/ServiceConfig'
     *    responses:
     *      '200':
     *        description: JSON containing the list of added service paths
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/ServicePaths'
     *      '500':
     *        $ref: '#/components/responses/StringError'
     *    externalDocs:
     *      description: OpenRPC specification
     *      url: https://spec.open-rpc.org
     */
    router.post('/', (req, res) => {
      const paths = [];

      const doc = req.body;
      forEach(doc?.servers, (server) => {
        try {
          if (!server.url && !server.name) {
            return;
          }
          else if (this.hasService(server.name)) {
            debug("unable to add the service '%s': already exists",
              server.name);
            return;
          }

          const s = this.getService(server.url, /* create = */ true);
          this.addAlias(server.url, server.name);
          paths.push(`/${s.name}`);
        }
        catch (err) {
          debug("unable to add the service (server url: %s): %s",
            server?.url, err.message);
          return;
        }
      });

      if (isEmpty(paths)) {
        res.status(500).send('No added service');
        return;
      }
      res.json({ paths });
    });

    /**
     * @openapi
     * /{host}/{command}:
     *  get:
     *    summary: send CRF JSON command
     *    tags: [ 'control' ]
     *    description: >
     *      Send a command to the intended CRF endpoint (host).
     *    parameters:
     *      - $ref: '#/components/parameters/Host'
     *      - $ref: '#/components/parameters/Command'
     *      - $ref: '#/components/parameters/QueryWriterIdCheck'
     *      - $ref: '#/components/parameters/QueryCommandIdCheck'
     *      - $ref: '#/components/parameters/QueryStream'
     *      - $ref: '#/components/parameters/QueryTimeout'
     *      - $ref: '#/components/parameters/QueryMessage'
     *    responses:
     *      '200':
     *        $ref: '#/components/responses/CRFCommandReply'
     *      '500':
     *        $ref: '#/components/responses/StringError'
     *    externalDocs:
     *      description: CERN Robotic Framework Protocol
     *      url: https://mro-dev.web.cern.ch/docs/std/cern-robotic-framework-protocol.html
     */
    router.get('/:host/:command', (req, res, next) => {
      if (lowerCase(req.get('connection')).includes('upgrade') &&
        lowerCase(req.get('upgrade')).includes('websocket')) {
        next('route'); // bypass this handler to start a WebSockets connection
        return;
      }
      this._httpHandler(
        req.params.host,
        assign(req.query, { command: req.params.command }), // command
        res
      );
    });

    /**
     * @openapi
     * /{host}/{command}:
     *  post:
     *    summary: send CRF JSON command
     *    tags: [ 'control' ]
     *    description: >
     *      Send a command to the intended CRF endpoint (host).
     *      <br><br>
     *      **Note:** the _message_ query parameter ovveride the
     *      property with the same name in the JSON command if defined.
     *    parameters:
     *      - $ref: '#/components/parameters/Host'
     *      - $ref: '#/components/parameters/Command'
     *      - $ref: '#/components/parameters/QueryWriterIdCheck'
     *      - $ref: '#/components/parameters/QueryCommandIdCheck'
     *      - $ref: '#/components/parameters/QueryStream'
     *      - $ref: '#/components/parameters/QueryTimeout'
     *      - $ref: '#/components/parameters/QueryMessage'
     *    requestBody:
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/components/schemas/JsonCommand'
     *    responses:
     *      '200':
     *        $ref: '#/components/responses/CRFCommandReply'
     *      '500':
     *        $ref: '#/components/responses/StringError'
     *    externalDocs:
     *      description: CERN Robotic Framework Protocol
     *      url: https://mro-dev.web.cern.ch/docs/std/cern-robotic-framework-protocol.html
     */
    router.post('/:host/:command', (req, res) => {
      this._httpHandler(
        req.params.host,
        assign(req.body, req.query, { command: req.params.command }), // command
        res
      );
    });

    /**
     * @openapi
     * /{host}/{command}:
     *  put:
     *    summary: send CRF JSON command
     *    tags: [ 'control' ]
     *    description: >
     *      Send a command to the intended CRF endpoint (host).
     *    parameters:
     *      - $ref: '#/components/parameters/Host'
     *      - $ref: '#/components/parameters/Command'
     *      - $ref: '#/components/parameters/QueryWriterIdCheck'
     *      - $ref: '#/components/parameters/QueryCommandIdCheck'
     *      - $ref: '#/components/parameters/QueryStream'
     *      - $ref: '#/components/parameters/QueryTimeout'
     *      - $ref: '#/components/parameters/QueryMessage'
     *    requestBody:
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/components/schemas/JsonCommand'
     *    responses:
     *      '200':
     *        $ref: '#/components/responses/CRFCommandReply'
     *      '500':
     *        $ref: '#/components/responses/StringError'
     *    externalDocs:
     *      description: CERN Robotic Framework Protocol
     *      url: https://mro-dev.web.cern.ch/docs/std/cern-robotic-framework-protocol.html
     */
    router.put('/:host/:command', (req, res) => {
      this._httpHandler(
        req.params.host,
        assign(req.body, req.query, { command: req.params.command }), // command
        res
      );
    });

    if (!router.ws) {
      console.warn(
        'Impossible to create stream commands: WebSocket not supported');
      return;
    }

    /**
     * @openapi
     * /{host}:
     *  get:
     *    summary: >
     *      open a WebSocket to communicate with the specified CRF endpoint
     *    tags: [ 'control', 'websocket' ]
     *    description: >
     *      Open a WebSocket to send multiple CRF JSON commands and receive
     *      CRF Replies or CRF packets (if they contain frames).
     *      <br><br>
     *      Once opened commands can easily be sent as shown in the examples
     *      below:
     *      <br><br>
     *      ```
     *      ws.send(JSON.stringify({ command: "getStatus" }));
     *      ```
     *      <br><br>
     *      ```
     *      ws.send(JSON.stringify({ command: "setStatus", message: { ... } }))
     *      ```
     *
     *    parameters:
     *      - $ref: '#/components/parameters/Host'
     *      - $ref: '#/components/parameters/QueryWriterIdCheck'
     *      - $ref: '#/components/parameters/QueryCommandIdCheck'
     *    responses:
     *      '101':
     *        description: Open a WebSocket.
     *      '500':
     *        $ref: '#/components/responses/StringError'
     *    externalDocs:
     *      description: CERN Robotic Framework Protocol
     *      url: https://mro-dev.web.cern.ch/docs/std/cern-robotic-framework-protocol.html
     */
    router.ws('/:host', async (ws, req, next) => {
      const service = req.params.host;
      debug("WebSocket opening request for service '%s' (query: %o)",
        service, req.query);

      /** @type {Service|null} */
      let srv = null;

      try {
        srv = this.getService(service, /* create = */ true);
        if (!srv) { throw new Error(`Failed to create service: ${service}`); }
      }
      catch (err) {
        console.error(
          `WebSocket error [service: ${service}]:`, err.message ?? err);
        ws.close(WS_INTERNAL_ERROR, err.message ?? err);
        return;
      }

      // @ts-ignore: set the default value if 'undefined'
      const timeout = req.query?.timeout > 0 ? req.query.timeout * 1000 :
        ServicesManager.DEFAULT_TIMEOUT;

      const crfStream = srv.getStream(req.query, timeout);

      crfStream
      .then((stream) => {
        stream
        .on('data', (data) => ws.send(data))
        .on('error', (err) => ws.send(JSON.stringify({
          type: 'error',
          data: err.message ?? err
        })))
        .once('close', () => {
          stream.removeAllListeners();
          ws.close(WS_GOING_AWAY);
        });
      })
      .catch((err) => ws.close(WS_INTERNAL_ERROR, err.message));

      ws
      .on('message', async (/** @type {string} */data) => {
        try {
          const cmd = JSON.parse(data);
          if (!(await crfStream).sendCmd(cmd)) {
            throw new Error(
              `Unable to send commands on the stream [service: ${service}]`);
          }
        }
        catch (err) {
          ws.send(JSON.stringify({ type: 'error', data: err.message }));
        }
      })
      .on('error', (err) => {
        ws.close(WS_INTERNAL_ERROR, err.message);
      })
      .once('close', () => {
        ws.removeAllListeners();
        crfStream.then((stream) => stream.close()).catch(next);
      });
    });
  }

  /**
   * @param {string|Buffer} data
   * @return {string|Buffer}
   */
  _prepareChunk(data) {
    const chunkSize = (size(data)).toString(16) + '\r\n';
    if (isString(data)) {
      return  chunkSize + data + '\r\n';
    }
    else if (isBuffer(data)) {
      return Buffer.concat(
        [ Buffer.from(chunkSize), data, Buffer.from('\r\n') ],
        chunkSize.length + data.length + 2);
    }
    else {
      throw new Error('Invalid chunk-data');
    }
  }

  /**
   * @param {string} service
   * @param {any} command
   * @param {Response} res
   */
  async _httpHandler(service, command, res) {
    debug("HTTP %s request for service '%s' (query: %o)",
      res.req?.method, service, command);
    try {
      const srv = this.getService(service, /* create = */ true);
      if (!srv) { throw new Error(`Failed to create service: ${service}`); }

      // NOTE: the command 'message' can be a url-encoded string
      const timeout = parseCommandMessage(command)?.timeout > 0 ?
        command.timeout * 1000 : ServicesManager.DEFAULT_TIMEOUT;

      if (some([ 'true', '1' ], (v) => toLower(command?.stream) === v)) {
        // streaming (chuncked reply)

        // get a stream (passing 'command' which may contain stream config)
        const crfStream = await srv.getStream(command, timeout);

        /** @type {(chunk: string|Buffer) => any} */
        const sendChunk = (chunk) => {
          try {
            if (res.writable) {
              res.write(this._prepareChunk(chunk));
            }
            else {
              throw new Error('response is no longer writable');
            }
          }
          catch (err) {
            console.error(`Unable to send the chunk [service: ${service}]:`,
              err.message, '(data:', isString(chunk) ?
                // preview
                chunk.slice(0, 50) : chunk?.toString?.('utf8', 0, 50) + '...)');
          }
        };

        res
        .on('error', (err) => sendChunk(
          JSON.stringify({ type: 'error', data: err.message })))
        .once('close', () => {
          res.removeAllListeners();
          crfStream.close();

          if (!res.writableEnded) { res.end(); }
        })
        .status(200)
        .set('Transfer-Encoding', 'chunked')
        .flushHeaders();

        crfStream
        .on('data', (data) => sendChunk(data))
        .on('error', (err) => sendChunk(
          JSON.stringify({ type: 'error', data: err.message ?? err })
        ))
        .once('close', () => {
          crfStream.removeAllListeners();

          if (!res.writableEnded) { res.end(); }
        });

        if (!crfStream.sendCmd(command)) {
          sendChunk(JSON.stringify({
            type: 'error',
            data: `Unable to send commands on the stream [service: ${service}]`
          }));
        }
      }
      else {
        // single reply
        res.send(await srv.getCmdReply(command, timeout));
      }
    }
    catch (err) {
      console.error(
        `HTTP handler error [service: ${service}]:`, err.message || err.code);
      res.status(500).send(err.message || err.code);
    }
  }

  release() {
    this._serviceMap.forEach((service) => service.destroy());
    this._serviceMap.clear();

    this._router = null;

    this._swagger?.release();
    this._swagger = null;
  }

  /**
   * @param {string} name
   * @returns {boolean}
   */
  hasService(name) {
    name = toLower(name);
    name = this._aliasMap.get(name) ?? name;
    return this._serviceMap.has(name);
  }


  /**
   * @brief get a service by creating it if it does not exist and
   *  'create' is true
   * @param {string} service hostname or service name
   * @param {boolean} [create=false] try to create the service
   * @param {ServiceConfig} [config] to apply when creating the service
   * @returns {Service|null}
   */
  getService(service, create = false, config) {
    service = toLower(service);
    const serviceName = this._aliasMap.get(service) ?? service;

    let srv = this._serviceMap.get(serviceName) ?? null;
    if (!srv && create) {
      const host = split(service, ':');
      if (host.length !== 2 || !host[0] || !host[1]) {
        throw new Error(`Unknown service: ${service}`);
      }
      srv = new Service(host[0], toInteger(host[1]), service, config);
      this._serviceMap.set(service, srv);
      debug("service '%s' added", service);

      if (this._swagger) {
        // add dynamic api to swagger doc
        const doc = {};
        set(doc, [ 'paths', `/${service}/{command}` ], httpAPI(service));
        set(doc, [ 'paths', `/${service}` ], wsAPI(service));
        this._swagger.addExtraDoc(doc);
      }
    }
    return srv;
  }

  /**
   * @param {string} host
   * @param {string} name
   * @returns {boolean} true if alias was added
   */
  addAlias(host, name) {
    if (!name || !isString(name)) { return false; }
    name = toLower(name);

    const srv = this._serviceMap.get(host);
    if (srv) {
      srv.name = name; // update the service name
      this._aliasMap.set(name, host);
      debug("alias added for service: %s, alias: %s", host, name);

      // update swagger doc
      if (this._swagger) {
        const doc = {};
        // Note: '~1' -> escapes '/'
        // (see https://swagger.io/docs/specification/using-ref/#escape)
        set(doc, [ 'paths', `/${name}/{command}`, '$ref' ],
          `#/paths/~1${host}~1{command}`);
        set(doc, [ 'paths', `/${name}`, '$ref' ], `#/paths/~1${host}`);
        this._swagger.addExtraDoc(doc);
      }
      return true;
    }
    return false;
  }
}
ServicesManager.DEFAULT_TIMEOUT = 10000; // ms

module.exports = ServicesManager;
