// @ts-check

const
  Server = require('./Server'),
  CRFSocket = require('./CRFSocket'),
  CRFPacket = require('./Packets/CRFPacket').CRFPacket,
  CRFPacketType = require('./Packets/CRFPacket').PacketType,
  ServicesManager = require('./ServicesManager'),
  // Mocks
  CRFMockServer = require('./mocks/CRFServer'),
  MissionManager = require('./mocks/Interfaces/MissionManager');

const port = 3000;

if (require.main === module) {
  (async function() {
    const server = new Server({ port });
    /* we're called as a main, let's listen */
    await server.listen(() => {
      console.log(`Server listening on port http://localhost:${port}`);
    });
  }());

}
else {
  module.exports = {
    CRFSocket, ServicesManager,
    CRFPacket, CRFPacketType,
    CRFMockServer,
    Mocks: { MissionManager }
  };
}

