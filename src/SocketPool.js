// @ts-check

const { makeDeferred, timeout } = require("@cern/prom");
const { noop } = require('lodash');
const CRFSocket = require("./CRFSocket");
const debug = require('debug')('crf:socketpool');

/**
 * @typedef {CRFExpress.ServiceConfig} SocketConfig
 */


class SocketPool {

  /**
   * @param {string} serviceName
   * @param {SocketConfig} [config]
   */
  constructor(serviceName, config) {
    this.serviceName = serviceName;
    this._nbrSocket = // @ts-ignore: 'socketNumber' is checked
      config?.socketNumber > 0 ? config.socketNumber :
        SocketPool.SOCKET_NUMBER;

    this._config = config;

    this._socketUsed = 0;
    this._promId = 0;

    /** @type {Map<number, CRFSocket>} */
    this._busySocket = new Map();

    /** @type {Map<number, CRFSocket>} */
    this._freeSocket = new Map();

    /** @type {Map<number, prom.Deferred<CRFSocket>>} */
    this._promSocket = new Map();
  }

  /**
   * @returns {CRFSocket|undefined}
   */
  _createSocket() {
    const socket = new CRFSocket(this._config);

    debug('socket created [service: %s, socketId: %d]',
      this.serviceName, socket.id);
    ++this._socketUsed;

    socket.once('close', () => {
      // delete the socket from the 'freeSocket' map, if present in it
      if (!this._freeSocket.delete(socket.id) &&
        --this._socketUsed < 0) {
        console.error('socket closure: Invalid number of used sockets' +
          ` in service '${this.serviceName}' (${this._socketUsed})`);
      }

      // delete the socket from the 'busySocket' map, if present in it
      this._busySocket.delete(socket.id);

      if (this._promSocket.size > 0) {
        // acquire first promise
        const [ [ key, prom ] ] = this._promSocket.entries();
        this._promSocket.delete(key);
        if (prom?.isPending) {
          const socket = new CRFSocket(this._config);
          debug('socket created [service: %s, socketId: %d]',
            this.serviceName, socket.id);
          ++this._socketUsed;

          prom.resolve(socket);
        }
      }
    });

    return socket;
  }

  /**
   * @returns {CRFSocket|null}
   */
  _acquireFreeSocket() {
    const [ [ key, freeSocket ] ] = this._freeSocket.entries();
    this._freeSocket.delete(key);

    if (!freeSocket) { return null; }

    freeSocket.removeListener('error', noop);
    freeSocket.updateId();
    debug('free socket acquired [service: %s, socketId: %d]',
      this.serviceName, freeSocket.id);
    ++this._socketUsed;

    return freeSocket;
  }

  /**
   * @param {CRFSocket} socket
   */
  releaseSocket(socket) {
    if (!(socket instanceof CRFSocket) || !this._busySocket.has(socket.id) ||
      this._freeSocket.has(socket.id)) {
      return;
    }

    if (this._promSocket.size > 0) {
      // acquire first promise
      const [ [ key, prom ] ] = this._promSocket.entries();
      this._promSocket.delete(key);

      if (prom?.isPending) {
        socket.updateId();
        prom.resolve(socket);
        return;
      }
    }

    this._busySocket.delete(socket.id);
    // do not propagate any errors when socket is released
    socket.on('error', noop);
    this._freeSocket.set(socket.id, socket);
    debug('socket released [service: %s, socketId: %d]',
      this.serviceName, socket.id);

    if (--this._socketUsed < 0) {
      console.error('socket released: Invalid number of used sockets' +
        ` in service '${this.serviceName}' (${this._socketUsed})`);
    }
  }

  /**
   * @param {number} [ms=10000]
   * @returns {Promise<CRFSocket>}
   */
  async acquireSocket(ms = 10000) {
    let socket;

    if (this._freeSocket.size > 0) {
      socket = this._acquireFreeSocket();
    }
    else if (this._socketUsed < this._nbrSocket) {
      socket = this._createSocket();
    }
    else {
      debug("Max number of sockets reached for the service '%s' (%d) ",
        this.serviceName, this._nbrSocket);
    }

    if (!socket) { // try to get one later
      const id = this._promId;
      const def = makeDeferred();

      def.abort = () => this._promSocket.delete(id);

      socket = timeout(def, ms, 'Unable to acquire a socket (time out)');
      socket.then((sckt) => {
        debug('promised socket acquired [service: %s, socketId: %d]',
          this.serviceName, sckt.id);
        this._busySocket.set(sckt.id, sckt);
      }).catch(noop);

      this._promSocket.set(id, def);
      ++this._promId;
    }
    else {
      this._busySocket.set(socket.id, socket);
    }

    return socket;
  }

  destroy() {
    // close all socket in free socket and prom socket
    this._promSocket.forEach((p) => {
      if (p.isPending) {
        p.reject(new Error(
          `Destroying the socket pool [service: ${this.serviceName}]`));
      }
    });
    this._promSocket.clear();

    this._freeSocket.forEach((s) => s.close());
    this._freeSocket.clear();

    this._busySocket.forEach((s) => s.close());
    this._busySocket.clear();

    this._socketUsed = 0;
  }
}

SocketPool.SOCKET_NUMBER = 5;

module.exports = SocketPool;
