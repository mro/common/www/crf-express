// @ts-check

const
  debug = require('debug')('crf:mock:server'),
  { isFunction, isString, isEmpty, isNil, toString } = require('lodash'),

  VarBuffer = require('../utils/VarBuffer'),
  TCPServer = require("../../test/utils/TCPServer"),
  { CRFPacket, PacketType } = require('../Packets/CRFPacket');


let _port = 4000;

class CRFServer {
  /** @type {string} */
  #name;

  /** @type {number} */
  #port;

  /** @type {TCPServer} */
  #server;

  /** @type {Promise<boolean>} */
  #listening;

  /** @type {Map<string, (args: any[]) => any>} */
  #handlers;

  /** @type {Map<number, number>} */
  #clients;

  /** @type {VarBuffer} */
  #buffer = new VarBuffer();

  /**
   * @param {string} name
   * @param {number} [port]
   */
  constructor(name, port) {
    this.#port = port ?? _port++;
    this.#name = name ?? `crf-server-${this.#port}`;
    this.#server = new TCPServer();

    this.#handlers = new Map();
    this.#clients = new Map();

    this.#listening = this.#server.listen(this.#port, () => {
      this.#server.on('connection', (socketId) => {
        const sckt = this.#server.getSocketById(socketId);
        sckt
        ?.on('data', (data) => {
          debug('received data: %s', toString(data));

          // packet splitting
          const packets = this.#getPackets(data, socketId);
          packets.forEach((packet) => {
            if (isNil(packet)) {
              const err = new Error('Invalid packet. Packet discarded');
              debug('error: %s [socketId: %d]', err.message, socketId);
              return;
            }

            // store the lastest writerID
            const writerID = packet.getHeader().writerID;
            this.#clients.set(socketId, writerID);
            // run JSON command
            const json = packet.getJSON();

            const cmdName = json?.command;
            const message = json?.message;
            delete json.command;
            delete json.message;

            this.#runHandler(cmdName, { ...message, ...json });
          });
        })
        ?.on('error', (err) => {
          debug('error: %s [socketId: %d]', err.message, socketId);
        })
        ?.once('close', (hadError) => {
          sckt.removeAllListeners();

          debug('closed (hadError: %s) [socketId: %d]',
            hadError ? 'true' : 'false', socketId);
        });
      });

      debug("'%s' ready", this.#name);
      return true;
    })
    .catch((err) => {
      console.error(`'${this.#name}' error: ${err}`);
      return false;
    });
  }

  destroy() {
    this.#server.removeAllListeners();
    this.#handlers.clear();

    this.#listening.then((value) => {
      if (value) { this.#server.close(); }
    });
  }

  /**
   * @param {Buffer} data
   * @returns {Array<CRFPacket|null>} // 'null' for invalid packets
   */
  #getPackets(data, socketId) {
    if (isEmpty(data)) { return []; }

    this.#buffer.add(data);

    const packets = [];
    while (this.#buffer.length >= CRFPacket.HEADER_LENGTH) {

      // check for a CRF Packet deserializing the whole buffer
      const desPacket = CRFPacket.deserialize(this.#buffer.buffer());

      if (!isNil(desPacket)) {
        const payloadLen = desPacket.getHeader().length;

        const pcktLen = CRFPacket.HEADER_LENGTH + payloadLen;
        if (this.#buffer.length < pcktLen) {
          debug("incomplete packet [socketId: %d]: received '%d' of '%d' bytes",
            socketId, this.#buffer.length, pcktLen);
          break;
        }

        packets.push(CRFPacket.deserialize(Buffer.from(
          this.#buffer.slice(0, pcktLen))));

        debug('packet extracted (#%d) [socketId: %d]',
          packets.length, socketId);
        this.#buffer.shift(CRFPacket.HEADER_LENGTH + payloadLen);
      }
      else {
        const index = this.#buffer.buffer().indexOf(CRFPacket.SYNC_PATTERN);

        this.#buffer.shift(index === -1 ?
          // clean buffer retaining the last 'SYNC_PATTERN -1' bytes (worst case)
          this.#buffer.length - (CRFPacket.SYNC_PATTERN.length - 1) :
          // clean buffer till the next SYNC_PATTERN
          index);

        packets.push(null);
        debug('data rejected [socketId: %d]: packet ill-formed', socketId);
      }
    }

    return packets;
  }

  /**
   * @param {any} data
   * @param {PacketType} [type]
   */
  sendPacket(data, type) {
    this.#clients.forEach((writerID, socketId) => {
      const packet =
        new CRFPacket(type ?? PacketType.NONE, writerID, data, Date.now());
      this.#server.send(packet.serialize(), socketId);
    });
  }

  /**
   * @param {string} name
   * @param {(any) => any} cb
   * @returns {boolean} - true if the method has been registered
   */
  registerHandler(name, cb) {
    if (!isString(name || null) || !isFunction(cb) ||
      this.#handlers.has(name)) {
      return false;
    }
    this.#handlers.set(name, cb);
    return true;
  }

  /**
   * @param {string} name
   * @returns {boolean} - true if the method existed and has been removed
   */
  unregisterHandler(name) {
    return this.#handlers.delete(name);
  }

  /**
   * @param {string} name
   * @param  {...any} args
   */
  #runHandler(name, ...args) {
    // @ts-ignore: args have to be spread
    this.#handlers.get(name)?.(...args);
  }
}

module.exports = CRFServer;
