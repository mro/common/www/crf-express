// @ts-check

const
  debug = require('debug')('crf:mock:base:device'),

  { PacketType } = require('../../Packets/CRFPacket'),
  CRFServer = require("../CRFServer");


class BaseDevice {
  /** @type {string} */
  #name;

  /** @type {CRFServer} */
  #crfServer;

  /** @type {Map<string, (args: any[]) => any>} */
  #handlers = new Map();

  /** @type {string[]} */
  #registeredHandlers = [];

  /**
   * @param {string} name
   */
  constructor(name) {
    this.#name = name;
  }

  /** @param {CRFServer} server */
  register(server) {
    if (!(server instanceof CRFServer)) { return; }
    this.#crfServer = server;

    debug("registrering '%s'...", this.#name);

    this.#handlers.forEach((handler, key) => {
      if (this.#crfServer.registerHandler(key, handler)) {
        this.#registeredHandlers.push(key);
        debug("'%s:%s' handler registered", this.#name, key);
      }
      else {
        debug("cannot register '%s:%s' handler", this.#name, key);
      }
    });
  }

  unregister() {
    if (!this.#crfServer || this.#registeredHandlers.length === 0) { return; }
    debug("unregistering '%s'...", this.#name);

    this.#registeredHandlers.forEach((name) => {
      if (this.#crfServer.unregisterHandler(name)) {
        debug("'%s:%s' handler unregistered", name, this.#name);
      }
      else {
        debug("cannot unregister '%s:%s' handler", name, this.#name);
      }
    });

    this.#registeredHandlers = [];
  }

  /**
   * @param {string} name
   * @param {(args: any[]) => any} handler
   * @returns {boolean} - false if the handler already exists
   */
  addHandler(name, handler) {
    if (this.#handlers.has(name)) { return false; }
    this.#handlers.set(name, handler);
    return true;
  }

  /**
   * @param {string} name
   * @returns {boolean} - true if the handler existed and has been removed
   */
  removeHandler(name) {
    return this.#handlers.delete(name);
  }

  removeAllHandlers() {
    this.#handlers.clear();
  }

  /**
   * @brief send JSON reply/error
   * @param {string} cmd reply command name
   * @param {any} message
   * @param {any} [extra] other fields (e.g. id)
   */
  sendJson(cmd, message, extra) {
    this.#crfServer?.sendPacket({
      command: 'reply',
      replyCommand: cmd,
      message,
      ...extra
    }, PacketType.JSON);
  }

  /**
   * @param {Buffer} data
   */
  sendFrame(data) {
    this.#crfServer?.sendPacket(data, PacketType.FRAME);
  }
}


module.exports = BaseDevice;
