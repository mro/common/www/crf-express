// @ts-check

const
  { pull, isFunction, isNil } = require('lodash'),
  debug = require('debug')('crf:mock:mission:manager'),

  BaseDevice = require('./BaseDevice');

class MissionManager extends BaseDevice {
  /** @type {number} */
  streamMode = MissionManager.STREAM_MODE.UNKNOWN;

  /** @type {number} */
  streamFrequency = 0; // Hz (message/sec)

  /** @type {NodeJS.Timer|null} */
  #streamTimer = null;

  /**
   * @param {string} name
   */
  constructor(name) {
    super(name);

    const handlers =
      pull(Object.getOwnPropertyNames(MissionManager.prototype),
        // list of prototype methods to be excluded
        ...[ 'constructor', 'destroy' ]);

    for (const key of handlers) {
      if (!isFunction(this[key])) {
        continue;
      }
      else if (this.addHandler(key, this[key].bind(this))) {
        debug("'%s' handler added to '%s'", key, name);
      }
      else {
        debug("cannot add '%s' handler to '%s'", key, name);
      }
    }
  }

  destroy() {
    clearInterval(/** @type {NodeJS.Timer}*/(this.#streamTimer));
    super.unregister();
    super.removeAllHandlers();
  }

  // --- Handlers ---

  start() {
    debug('start');
  }

  next() {
    debug('next');
  }

  stop() {
    debug('stop');
  }

  pause() {
    debug('pause');
  }

  resume() {
    debug('resume');
  }

  goHome() {
    debug('goHome');
  }

  recharge() {
    debug('recharge');
  }

  getStatus() {
    debug('getStatus');
  }

  /** @param {any} status */
  setStatus(status) {
    debug('setStatus: %o', status);
  }

  emergency() {
    debug('emergency');
  }

  rearm() {
    debug('rearm');
  }

  startStreamStatus(status) {
    if (this.#streamTimer) { return; }
    debug('startStreamStatus: %o', status);

    this.streamMode = status['mode'];
    this.streamFrequency = status['frequency'];
    if (this.streamMode === MissionManager.STREAM_MODE.FREQUENCY) {
      this.#streamTimer = setInterval(() => this.getStatus(),
        (1 / this.streamFrequency) * 1000);
    }
  }

  stopStreamStatus() {
    if (isNil(this.#streamTimer)) { return; }
    debug('stopStreamStatus');

    clearInterval(this.#streamTimer);
    this.#streamTimer = null;
  }
}

MissionManager.STREAM_MODE = {
  UNKNOWN: 0,
  FREQUENCY: 1, // send status periodically
  DIFFERENTIAL: 2 // send status upon updates
};

module.exports = MissionManager;
