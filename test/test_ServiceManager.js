// @ts-check

/* eslint-disable max-lines */

const
  { describe, it, afterEach, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  sa = require('superagent'),
  WebSocket = require('ws'),

  { TCPServer, waitForEvent, waitForValue } = require('./utils'),
  { CRFPacket, PacketType } = require('../src/Packets/CRFPacket'),
  Server = require('../src/Server');


describe('ServiceManager', function() {
  const env = {
    serverPort: 3000,
    crfPort: 4000
  };

  beforeEach(async function() {
    env.crfServer = new TCPServer();
    await env.crfServer.listen(env.crfPort);

    env.server = new Server({ port: env.serverPort });
    await env.server.listen();

    /** @type {CRFExpress.ServicesManager} */
    env.servicesManager = env.server.servicesManager;
  });

  afterEach(function() {
    if (env.wsCli?.readyState === WebSocket.OPEN) {
      env.wsCli.close();
      delete env.wsCli;
    }

    env.server.close();
    env.crfServer?.close();

    delete env.server;
    delete env.crfServer;
    delete env.servicesManager;
  });

  it('can add/get/check services', function() {
    const srv = env.servicesManager.getService('localhost:4000', true); // create = true
    expect(env.servicesManager.hasService('localhost:4000')).to.be.true();

    env.servicesManager.addAlias('localhost:4000', 'ARIS');
    expect(env.servicesManager.hasService('ARIS')).to.be.true();

    expect(srv).to.be.equal(env.servicesManager.getService('ARIS'));
  });

  it('can create a new service', async function() {
    const
      srvHost = `localhost:${env.serverPort}`;

    try {
      // create the service
      const ret = await sa.post(`http://${srvHost}`)
      .type('json')
      .send({ // OpenRPC doc
        "openrpc": "1.2.1",
        "info": {
          "title": "Service Introspection Example",
          "version": "1.0.0"
        },
        "servers": [
          { "name": "sample-service",
            "url": "192.168.1.1:1234" }
        ],
        "methods": [
          { "name": "moveToTarget",
            "description": "set the target position and velocity",
            "params": [
              { "name": "position", "schema": { "type": "number" } },
              { "name": "velocity", "schema": { "type": "number" } }
            ],
            "result": {
              "name": "moveToTargetReply",
              "schema": { "type": "boolean" }
            }
          }
        ]
      });

      expect(ret.status).to.be.equal(200);
      expect(ret.body).to.be.deep.equal({ paths: [ '/sample-service' ] });
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.response?.text || err.message}`);
    }
  });

  it('can create service and perform a command in one shot', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'setStatus';

    try {
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 0, command: 'reply', replyCommand: command,
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());

      const ret = await
      sa.put(`http://${srvHost}/${crfHost}/${command}`)
      .type('json')
      .send({ param1: 123, param2: 'test', param3: false,
        writerIdCheck: false });

      expect(ret.status).to.be.equal(200);
      expect(ret.body.message).to.be.deep.equal({ status: 'OK' });
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.response?.text || err.message}`);
    }
  });

  it('can serve GET command', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'getStatus';

    try {
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 1, command: 'reply', replyCommand: command,
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());

      const ret = await
      sa
      .get(`http://${srvHost}/${crfHost}/${command}` +
        '?message[param1]=test&message[param2][0]=1&message[param2][1]=2' +
        '&id=1&writerIdCheck=false'
      )
      .type('json');

      expect(ret.body.message).to.be.deep.equal({ status: 'OK' });
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.response?.text || err.message}`);
    }
  });

  it('can serve POST command', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'setStatus';

    try {
      const json = { param1: 123, param2: 'test', param3: false,
        param4: [ 1, 2, 3, 4 ], cmdIdCheck: false, writerIdCheck: false };
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          command: 'reply', replyCommand: command,
          message: json
        },
        Date.now()
      )).serialize());

      const ret = await
      sa.post(`http://${srvHost}/${crfHost}/${command}`).send(json);

      expect(ret.body.message).to.be.deep.equal(json);
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.response?.text || err.message}`);
    }
  });

  it('can serve PUT HTTP request', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'setStatus';

    try {
      const json = { id: 1, param1: 123, param2: 'test', param3: false,
        param4: [ 1, 2, 3, 4 ], writerIdCheck: false };
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 1, command: 'reply', replyCommand: command,
          message: json
        },
        Date.now()
      )).serialize());

      const ret = await
      sa.put(`http://${srvHost}/${crfHost}/${command}`)
      .type('json')
      .send(json);

      expect(ret.body.message).to.be.deep.equal(json);
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.response?.text || err.message}`);
    }
  });

  it('can serve (several) JSON commands using WebSocket', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`;

    try {
      env.wsCli =
        new WebSocket(`ws://${srvHost}/${crfHost}?writerIdCheck=false`);

      await waitForEvent(env.wsCli, 'open');

      // fist command
      let message = waitForEvent(env.wsCli, 'message');
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          command: 'reply', replyCommand: 'getStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());

      env.wsCli.send(JSON.stringify({ command: 'getStatus' }));

      let reply = await message;
      expect(reply).to.be.equal(JSON.stringify({
        command: 'reply',
        replyCommand: 'getStatus',
        message: { status: 'OK' }
      }));

      message = waitForEvent(env.wsCli, 'message');
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          command: 'reply', replyCommand: 'setStatus',
          message: { ok: true }
        },
        Date.now()
      )).serialize());

      env.wsCli.send(JSON.stringify({
        command: 'setStatus',
        message: { param: 'test' }
      }));

      reply = await message;
      expect(reply).to.be.equal(JSON.stringify({
        command: 'reply',
        replyCommand: 'setStatus',
        message: { ok: true }
      }));
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });

  it('can stream frames through WebSocket', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`;

    try {
      env.wsCli =
        new WebSocket(`ws://${srvHost}/${crfHost}?writerIdCheck=false`);

      await waitForEvent(env.wsCli, 'open');

      const message = waitForEvent(env.wsCli, 'message');
      const pckt = new CRFPacket(
        PacketType.FRAME,
        0,
        Buffer.from('FRAME TEST'),
        Date.now()
      );
      env.crfServer.prepareReply(pckt.serialize());
      env.wsCli.send(JSON.stringify({ command: 'getFrame' }));

      const reply = await message;
      expect(reply).to.be.deep.equal(pckt.serialize());
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });

  it('can stream over HTTP', async function() {
    let error = null;
    let received = '';
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'getStatus';

    try {
      // retrieve the server-side socket id
      const srvSocktetId = waitForEvent(env.crfServer, 'connection');

      sa
      .get(`http://${srvHost}/${crfHost}/${command}?writerIdCheck=0&stream=1`)
      .buffer(false)
      .parse((res) => {
        res
        .on('error', (err) => { error = err; })
        .on('data', (chunk) => { received += chunk; })
        .once('close', () => res.removeAllListeners());
      })
      .end();

      const srvSocket = env.crfServer.getSocketById(await srvSocktetId);

      // receive multiple frames from CRF-server
      let expectedFrames = '';
      for (let i = 0; i < 3; ++i) {
        const frame = `Frame --- ${i}`;

        const pckt = new CRFPacket(PacketType.FRAME, 0,
          Buffer.from(frame), Date.now());

        const serializedPacket = pckt.serialize();

        expectedFrames += `${serializedPacket.length.toString(16)}\r\n` +
          `${serializedPacket.toString()}\r\n`;

        srvSocket.write(serializedPacket);
      }
      await waitForValue(() => received, expectedFrames,
        `Should receive the frames: ${expectedFrames}`);
      received = ''; // clean

      // receive multiple json reply from CRF-server
      let expectedJSONs = '';
      for (let i = 0; i < 3; ++i) {
        const json = JSON.stringify({ param: i });
        expectedJSONs += `${json.length.toString(16)}\r\n${json}\r\n`;

        const pckt = new CRFPacket(PacketType.JSON, 0, json, Date.now());

        srvSocket.write(pckt.serialize());
      }
      await waitForValue(() => received, expectedJSONs,
        `Should receive the JSONs: '${expectedJSONs}'`);
      received = ''; // clean

      if (error) { throw error; }
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });

  it('can receive error when streaming over HTTP', async function() {
    let error = null;
    let received = '';
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'getStatus';

    try {
      env.crfServer.prepareReply(new CRFPacket(
        PacketType.JSON,
        200, // invalidate the writer id (too high)
        { status: 'OK' }, // JSON reply
        Date.now()
      ).serialize());

      sa
      .get(`http://${srvHost}/${crfHost}/${command}?stream=1`)
      .buffer(false)
      .parse((res) => {
        res
        .on('error', (err) => { error = err; })
        .on('data', (chunk) => { received += chunk; })
        .once('close', () => res.removeAllListeners());
      })
      .end();


      const err = JSON.stringify({ type: 'error',
        data: 'writerID mismatch. Closing socket...' });
      const expectedError = `${err.length.toString(16)}\r\n${err}\r\n`;

      await waitForValue(() => received, expectedError,
        `Should receive an error: ${expectedError}`);

      if (error) { throw error; }
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });

  it('can receive errors through WebSocket', async function() {
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`;

    try {
      env.wsCli = new WebSocket(`ws://${srvHost}/${crfHost}`);

      await waitForEvent(env.wsCli, 'open');

      // writer id mismatch error
      const error = waitForEvent(env.wsCli, 'message');
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        200,
        { // JSON reply
          command: 'reply', replyCommand: 'setStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());
      env.wsCli.send(JSON.stringify({ command: 'getStatus' }));

      const reply = await error;
      expect(reply).to.be.equal(JSON.stringify({
        type: 'error', data: 'writerID mismatch. Closing socket...'
      }));
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });

  it('allows to disable the JSON command and/or the writer id check',
    async function() {
      const
        srvHost = `localhost:${env.serverPort}`,
        crfHost = `localhost:${env.crfPort}`;

      try {
        // disable WriterID + JSON cmd Id in HTTP request using body
        env.crfServer.prepareReply((new CRFPacket(
          PacketType.JSON,
          210,  // ignored
          {
            id: 10, // ignored
            command: 'reply', replyCommand: 'setStatus',
            message: { status: 'OK' }
          },
          Date.now()
        )).serialize());

        const ret = await sa
        .post(`http://${srvHost}/${crfHost}/setStatus`)
        .send({ writerIdCheck: false, cmdIdCheck: false });

        expect(ret.body.message).to.be.deep.equal({ status: 'OK' });

        // disable WriterID in WebSocket using query string
        env.wsCli = new WebSocket(`ws://${srvHost}/${crfHost}?writerIdCheck=0`);

        await waitForEvent(env.wsCli, 'open');

        env.crfServer.prepareReply((new CRFPacket(
          PacketType.JSON,
          200,  // ignored
          {
            command: 'reply', replyCommand: 'getStatus',
            message: { status: 'OK' }
          },
          Date.now()
        )).serialize());

        const message = waitForEvent(env.wsCli, 'message');
        env.wsCli.send(JSON.stringify({ command: 'getStatus' }));

        const reply = await message;
        expect(reply).to.be.equal(JSON.stringify({
          command: 'reply',
          replyCommand: 'getStatus',
          message: { status: 'OK' }
        }));
      }
      catch (err) {
        expect.fail('Should not fail, but got the error: ' +
          `${err.response?.text || err.message}`);
      }
    });

  it('allows to set a timeout on the command response', async function() {
    let error = false;
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:${env.crfPort}`,
      command = 'getStatus';

    const start = Date.now();
    try {
      // NOTE: No reply prepared

      await sa
      .get(`http://${srvHost}/${crfHost}/${command}'?timeout=0.3` + // 300ms
        '&id=1&writerIdCheck=false'
      ).type('json');
    }
    catch (err) {
      const timeElapsed = Date.now() - start;
      error = true;
      expect(err.status).to.be.equal(500);
      expect(timeElapsed).to.be.greaterThan(290).and.lessThan(1000);
      expect(err.response.text).to.be.equal('CRF reply timeout');
    }

    if (!error) {
      expect.fail('Should fail because of the reply timeout');
    }
  });

  it('allows to send the JSON command message as a query parameter',
    async function() {
      const
        srvHost = `localhost:${env.serverPort}`,
        crfHost = `localhost:${env.crfPort}`,
        msg = { param1: 'foo', param2: [ 1, 2 ], param3: 3,
          param4: true, param5: { a: 'bar' } };

      env.crfServer.echo(true);

      try {
        // GET
        let ret = await sa.get(`http://${srvHost}/${crfHost}/setStatus` +
        `?message=${encodeURI(JSON.stringify(msg))}` +
        '&id=1&writerIdCheck=false');

        expect(ret.body.message).to.be.deep.equal(msg);

        // POST
        ret = await sa.post(`http://${srvHost}/${crfHost}/setStatus` +
        `?message=${encodeURI(JSON.stringify(msg))}`)
        .send({ message: { param1: 'OVERRIDDEN!!!' },
          writerIdCheck: false, cmdIdCheck: false });

        expect(ret.body.message).to.be.deep.equal(msg);

        // PUT
        ret = await sa.put(`http://${srvHost}/${crfHost}/setStatus` +
        `?message=${encodeURI(JSON.stringify(msg))}`)
        .send({ message: { param1: 'OVERRIDDEN!!!' },
          writerIdCheck: false, cmdIdCheck: false });

        expect(ret.body.message).to.be.deep.equal(msg);
      }
      catch (err) {
        expect.fail('Should not fail, but got the error: ' +
          `${err.response?.text || err.message}`);
      }
    });

  it('raises an error trying to create a wrong service', async function() {
    let error = false;
    const
      srvHost = `localhost:${env.serverPort}`,
      crfHost = `localhost:1234`, // wrong port
      command = 'getStatus';

    try {
      await sa.get(`http://${srvHost}/${crfHost}/${command}`);
    }
    catch (err) {
      error = true;
      expect(err.status).to.be.equal(500);
      expect(err.response.text).to.include('ECONNREFUSED');
    }

    if (!error) {
      expect.fail('Should fail because the CRF host is wrong');
    }
  });

  it('ignores wrong service server', async function() {
    const
      srvHost = `localhost:${env.serverPort}`;

    try {
      const ret = await sa.post(`http://${srvHost}`).type('json')
      .send({ // OpenRPC doc
        // "openrpc": "1.2.1",
        // ...
        "servers": [
          {
            "name": "sample-service-1",
            "url": ":5678"
          },
          {
            "name": "sample-service-2",
            "url": "192.168.1.1:1234"
          },
          {
            "name": "sample-service-3",
            "url": "192.168.1.200"
          }
        ]
        // "methods": [
        // ...
      });

      expect(ret.status).to.be.equal(200);
      expect(ret.body).to.be.deep.equal({ paths: [ '/sample-service-2' ] });
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });

  it('does not re-create an existing service', async function() {
    const
      srvHost = `localhost:${env.serverPort}`;

    try {
      let ret = await sa.post(`http://${srvHost}`).type('json')
      .send({ // OpenRPC doc
        // "openrpc": "1.2.1",
        // ...
        "servers": [
          { "name": "sample-service",
            "url": "192.168.1.1:1234" }
        ]
        // "methods": [
        // ...
      });

      expect(ret.status).to.be.equal(200);
      expect(ret.body).to.be.deep.equal({ paths: [ '/sample-service' ] });

      ret = await sa.post(`http://${srvHost}`).type('json')
      .send({ // OpenRPC doc
        // "openrpc": "1.2.1",
        // ...
        "servers": [
          { "name": "sample-service",
            "url": "192.168.1.1:1234" }
        ]
        // "methods": [
        // ...
      })
      .catch((err) => err);

      expect(ret.status).to.be.equal(500);
      expect(ret.response?.text).to.be.equal('No added service');

      expect(env.servicesManager._serviceMap.size).to.be.equal(1);
      expect(env.servicesManager.hasService('sample-service')).to.be.true();
      expect(env.servicesManager.hasService('192.168.1.1:1234')).to.be.true();
    }
    catch (err) {
      expect.fail('Should not fail, but got the error: ' +
        `${err.message ?? err}`);
    }
  });
});
