// @ts-check

const
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { expect } = require('chai'),
  { toString } = require('lodash'),
  TCPSocket = require("../src/TCPSocket"),
  { TCPServer, waitForEvent, waitForValue } = require('./utils');


describe("TCPSocket", function() {
  const env = {};

  beforeEach(async function() {
    env.server = new TCPServer();
    await env.server.listen();
  });

  afterEach(function() {
    env.client?.close();
    delete env.client;

    env.server?.close();
    delete env.server;
  });

  it('can connect to (disconnect from) a TCP server', async function() {
    env.client = new TCPSocket();

    try {
      const connection = waitForEvent(env.client, 'connected');
      env.client.connect('localhost', env.server?.port);
      await connection;

      const disconnection = waitForEvent(env.client, 'close');
      env.client.close();
      await disconnection;
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('can send and receive data', async function() {
    env.client = new TCPSocket();
    let cliData, srvData;

    try {
      const cliConnection = waitForEvent(env.client, 'connected');
      const socketId = waitForEvent(env.server, 'connection');
      env.client.connect('localhost', env.server?.port);

      await cliConnection;
      const srvSocket = env.server.getSocketById(await socketId);

      // from client to server
      srvSocket.once('data', (data) => { srvData = toString(data); });

      let prom = waitForValue(() => srvData, 'client2server',
        'Failed to send data from client to server');
      const ret = env.client.send('client2server');
      await prom;
      expect(ret).to.be.true();

      // from server to client
      env.client.once('data', (data) => { cliData = toString(data); });

      prom = waitForValue(() => cliData, 'server2client',
        'Failed to send data from server to client');
      srvSocket.write('server2client');
      await prom;
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('can catch errors', async function() {
    env.client = new TCPSocket();

    try {
      const connection = waitForEvent(env.client, 'connected');
      env.client.connect('localhost', env.server?.port);
      await connection;

      const error = waitForEvent(env.client, 'error');
      env.client._socket.destroy(new Error('ERROR TEST'));
      const errMsg = (await error).message;
      expect(errMsg).to.be.equal('ERROR TEST');
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('prevents sending data when not connected', function() {
    env.client = new TCPSocket();

    const ret = env.client.send('test');

    expect(ret).to.be.false();
  });

  it('prevents no data being sent', async function() {
    env.client = new TCPSocket();

    try {
      const connection = waitForEvent(env.client, 'connected');
      env.client.connect('localhost', env.server?.port);
      await connection;

      expect(env.client.send()).to.be.false();
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });
});
