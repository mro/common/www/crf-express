// @ts-check

const
  { afterEach, describe, it } = require('mocha'),
  { expect } = require('chai'),
  sa = require('superagent'),
  express = require('express'),
  { makeDeferred } = require('@cern/prom'),
  { spawn } = require('child_process'),

  { waitForValue } = require('./utils'),
  CRFExpress = require('../src');


describe('CRF-express', function() {
  const env = { port: 4567 };

  afterEach(function() {
    env.service?.release();
    delete env.service;

    env.server?.close();
    delete env.server;

    env.proc?.kill('SIGTERM');
    delete env.proc;
  });

  it('provides a CRF Proxy Service (ServiceManager)', async function() {
    const app = express();
    const def = makeDeferred();

    // create and register the CRF Proxy Service
    env.sm = new CRFExpress.ServicesManager();
    env.sm.register(app);

    env.server = app.listen(env.port, () => def.resolve(undefined));
    await def.promise;
  });

  it('provides a runnable CRF-Proxy', async function() {
    const def = makeDeferred();
    let expected = '';

    env.proc = spawn('node', [ 'src/index.js' ]);
    env.proc.once('error', def.reject);
    env.proc.once('spawn', def.resolve);
    env.proc.stdout.on('data', (chunk) => { expected += chunk; });

    const prom = waitForValue(() => {
      return expected
      .includes('Server listening on port http://localhost:3000');
    }, true);

    await Promise.all([ def.promise, prom ]);

    env.proc.stdout.removeAllListeners();
  });

  it('provides swagger and json open-api doc', async function() {
    const app = express();
    const def = makeDeferred();

    // create and register the CRF Proxy Service
    env.sm = new CRFExpress.ServicesManager();
    env.sm.register(app);

    env.server = app.listen(env.port, () => def.resolve(undefined));
    await def.promise;

    let ret = await sa.get(`http://localhost:${env.port}/api-docs`);

    expect(ret.status).to.be.equal(200);
    expect(ret.text).to.include('Swagger UI');


    ret = await sa.get(`http://localhost:${env.port}/api-docs.json`);
    expect(ret.status).to.be.equal(200);
    expect(ret.text).to.include('"openapi":"3.0.0"');
  });
});

