
const
  { expect } = require('chai'),
  { concat, map, partial } = require('lodash'),
  { describe, it } = require('mocha'),

  IOBuffer = require('../src/utils/IOBuffer');

describe('IOBuffer', function() {
  // eslint-disable-next-line max-statements
  function check(be) {
    var buff = Buffer.alloc(55);
    var bout = new IOBuffer.BufferOut(buff);

    bout.writeUInt8(0x77, be);
    bout.writeUInt16(0x1122, be);
    bout.writeUInt32(0x33445566, be);
    bout.writeUInt64(1234567n, be);

    bout.writeInt8(-12, be);
    bout.writeInt16(-123, be);
    bout.writeInt32(-1234, be);
    bout.writeInt64(-1234567n, be);

    bout.writeFloat(12.44, be);
    bout.writeDouble(123.44, be);

    bout.writeString("this is a te", 13);
    expect(bout.fail).to.equal(false);

    bout.writeUInt8(0);
    expect(bout.fail).to.equal(true);

    if (be) {
      expect(buff).to.deep.equal(Buffer.from(concat([
        0x77,
        0x11, 0x22,
        0x33, 0x44, 0x55, 0x66,
        0, 0, 0, 0, 0, 0x12, 0xd6, 0x87,
        0xF4,
        0xFF, 0x85,
        0xFF, 0xFF, 0xFB, 0x2E,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xed, 0x29, 0x79,
        0x41, 0x47, 0x0a, 0x3d,
        0x40, 0x5e, 0xdc, 0x28, 0xf5, 0xc2, 0x8f, 0x5c
      ],
      map("this is a te", (s) => s.charCodeAt(0)),
      0)
      ));
    }
    else {
      expect(buff).to.deep.equal(Buffer.from(concat([
        0x77,
        0x22, 0x11,
        0x66, 0x55, 0x44, 0x33,
        0x87, 0xd6, 0x12, 0, 0, 0, 0, 0,
        0xF4,
        0x85, 0xFF,
        0x2E, 0xFB, 0xFF, 0xFF,
        0x79, 0x29, 0xed, 0xff, 0xff, 0xff, 0xff, 0xff,
        0x3d, 0x0a, 0x47, 0x41,
        0x5c, 0x8f, 0xc2, 0xf5, 0x28, 0xdc, 0x5e, 0x40
      ],
      map("this is a te", (s) => s.charCodeAt(0)),
      0)
      ));
    }

    var bin = new IOBuffer.BufferIn(buff);
    expect(bin.readUInt8()).to.equal(0x77);
    expect(bin.readUInt16(be)).to.equal(0x1122);
    expect(bin.readUInt32(be)).to.equal(0x33445566);
    expect(bin.readUInt64(be)).to.deep.equal(BigInt(1234567));

    expect(bin.readInt8(be)).to.equal(-12);
    expect(bin.readInt16(be)).to.equal(-123);
    expect(bin.readInt32(be)).to.equal(-1234);
    expect(bin.readInt64(be)).to.deep.equal(BigInt(-1234567));
    expect(bin.readFloat(be)).to.be.closeTo(12.44, 0.001);
    expect(bin.readDouble(be)).to.be.closeTo(123.44, 0.001);

    expect(bin.readString(13)).to.equal("this is a te");
    expect(bin.fail).to.equal(false);

    expect(bin.readUInt32(be)).to.be.NaN();
    expect(bin.fail).to.equal(true);
  }

  it('can serialize little-endian data', partial(check, false));
  it('can serialize big-endian data', partial(check, true));
  it('defaults to little-endian', partial(check, undefined));

  describe('special cases', function() {
    it('serializes a null string', function() {
      var buff = Buffer.alloc(20);
      var out = new IOBuffer.BufferOut(buff);

      out.writeString(null, 2);
      out.writeString(undefined, 2);
      out.writeString('', 2);
      expect(out.idx).to.equal(6);
      expect(out.fail).to.equal(false);
    });
  });
});
