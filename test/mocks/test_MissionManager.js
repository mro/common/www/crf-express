// @ts-check

const
  { after, before, describe, it } = require('mocha'),
  { expect } = require('chai'),
  sinon = require('sinon'),

  { CRFPacket, PacketType } = require("../../src/Packets/CRFPacket"),
  CRFServer = require('../../src/mocks/CRFServer'),
  MissionManager = require('../../src/mocks/Interfaces/MissionManager'),
  CRFSocket = require('../../src/CRFSocket'),
  { waitForValue } = require('../utils');


describe('Mission Manager', function() {
  const env = { port: 4567 };
  const sandbox = sinon.createSandbox();

  before(async function() {
    env.server = new CRFServer('CRF Server test', env.port);
  });

  beforeEach(function() {
    sandbox.spy(MissionManager.prototype);
    env.mm = new MissionManager('MMTest');
    env.mm.register(env.server);

    env.client = new CRFSocket();
  });

  afterEach(function() {
    env.client.close();
    delete env.client;

    env.mm.destroy();
    delete env.mm;

    sandbox.restore();
  });

  after(function() {
    env.server?.destroy();
    delete env.server;
  });

  [
    'start', 'next', 'stop', 'pause', 'resume', 'goHome',
    'recharge', 'getStatus', 'setStatus', 'rearm', 'emergency',
    'startStreamStatus', 'stopStreamStatus'
  ]
  .forEach((handler) => {
    it(`can serve the '${handler}' command`, async function() {
      try {
        await env.client.connect('localhost', env.port);
        expect(env.mm[handler].notCalled).to.be.true();

        const prom = waitForValue(() => env.mm[handler].called, true);
        env.client.send(new CRFPacket(PacketType.JSON, env.client.id,
          { command: handler }, Date.now()));
        await prom;

        expect(env.mm[handler].calledOnce).to.be.true();
      }
      catch (err) {
        expect.fail(`Should not fail, but got the error: ${err.message}`);
      }
    });
  });

  it('can call getStatus with a given frequency', async function() {
    try {
      await env.client.connect('localhost', env.port);

      const prom = waitForValue(() => env.mm.getStatus.callCount, 2);

      const start = Date.now();
      env.client.send(new CRFPacket(PacketType.JSON, env.client.id,
        {
          command: 'startStreamStatus',
          mode: MissionManager.STREAM_MODE.FREQUENCY,
          frequency: 4.0 // Hz -> 2 times/sec
        }, Date.now()));
      await prom;
      env.mm.stopStreamStatus();
      expect(Date.now() - start).to.be.greaterThan(500).lessThan(1000);
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });
});
