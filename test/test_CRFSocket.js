// @ts-check
/* eslint-disable max-lines */

const
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { expect } = require('chai'),
  { replace, restore } = require('sinon'),

  CRFSocket = require("../src/CRFSocket"),
  { CRFPacket, PacketType } = require('../src/Packets/CRFPacket'),
  { TCPServer, waitForEvent } = require('./utils'),
  VarBuffer = require('../src/utils/VarBuffer');


describe("CRFSocket", function() {
  const env = {};

  beforeEach(async function() {
    replace(CRFSocket, 'BUFFER_CLEAN_TIMEOUT', 800); // ms

    env.server = new TCPServer();
    await env.server.listen();
  });

  afterEach(function() {
    env.client?.close();
    delete env.client;

    env.server?.close();
    delete env.server;

    restore();
  });

  it('can send/receive CRF JSON messages', async function() {
    env.client = new CRFSocket();
    const json = { a: 'foo' };

    try {
      const srvConn = waitForEvent(env.server, 'connection');
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;
      const srvSockId = await srvConn;

      const srvSocket = env.server.getSocketById(srvSockId);

      // send
      let prom = waitForEvent(srvSocket, 'data');
      let pckt = new CRFPacket(PacketType.JSON, env.client.id, json,
        Date.now());
      env.client.send(pckt);

      let data = await prom;
      expect(CRFPacket.deserialize(data)?.getJSON()).to.be.deep.equal(json);

      // receive
      prom = waitForEvent(env.client, 'packet');
      pckt = new CRFPacket(PacketType.JSON, env.client.id, json,
        Date.now());
      srvSocket.write(pckt.serialize());
      data = await prom;
      expect(data.getHeader()).to.be.deep.equal(pckt.getHeader());
      expect(data.getJSON()).to.be.deep.equal(json);
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('can send/receive CRF frames', async function() {
    env.client = new CRFSocket();
    const frame = Buffer.from('SAMPLE FRAME');

    try {
      const srvConn = waitForEvent(env.server, 'connection');
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;
      const srvSockId = await srvConn;

      const srvSocket = env.server.getSocketById(srvSockId);

      // send
      let prom = waitForEvent(srvSocket, 'data');
      let pckt = new CRFPacket(PacketType.FRAME, env.client.id, frame,
        Date.now());
      env.client.send(pckt);

      let data = await prom;
      expect(CRFPacket.deserialize(data)?.getPayload())
      .to.be.deep.equal(frame);

      // receive
      prom = waitForEvent(env.client, 'packet');
      pckt = new CRFPacket(PacketType.FRAME, env.client.id, frame,
        Date.now());
      srvSocket.write(pckt.serialize());
      data = await prom;
      expect(data.getHeader()).to.be.deep.equal(pckt.getHeader());
      expect(data.getPayload()).to.be.deep.equal(frame);

    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('can receive multiple packets in a row', async function() {
    env.client = new CRFSocket();

    try {
      const srvConn = waitForEvent(env.server, 'connection');
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;
      const srvSockId = await srvConn;

      const srvSocket = env.server.getSocketById(srvSockId);

      const nPackets = 3;

      // JSONs + FRAMEs
      const expected = [];
      const sendBuff = new VarBuffer();

      // prepare JSON packets
      for (let i = 0; i < nPackets; ++i) {
        const pckt = new CRFPacket(PacketType.JSON, env.client.id,
          { a: 'foo', n: i }, Date.now());
        expected.push(pckt);
        sendBuff.add(pckt.serialize());
      }

      // prepare FRAME packets
      for (let i = 0; i < nPackets; ++i) {
        const pckt = new CRFPacket(PacketType.FRAME, env.client.id,
          Buffer.from(`FRAME #${i}`), Date.now());
        expected.push(pckt);
        sendBuff.add(pckt.serialize());
      }

      const prom = waitForEvent(env.client, 'packet', nPackets);
      srvSocket.write(sendBuff.buffer());
      const data = await prom;

      data.forEach((pckt, i) => {
        if (pckt.getJSON()) { // JSON packet
          expect(pckt.getJSON()).to.be.deep.equal(expected[i].getJSON());
        }
        else {
          expect(pckt.getPayload()).to.be.deep.equal(expected[i].getPayload());
        }
      });
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('retains incomplete packets for a certain amount of time',
    async function() {
      env.client = new CRFSocket();

      try {
        const srvConn = waitForEvent(env.server, 'connection');
        const cliConn = waitForEvent(env.client, 'connected');

        env.client.connect('localhost', env.server?.port);
        await cliConn;
        const srvSockId = await srvConn;

        const srvSocket = env.server.getSocketById(srvSockId);

        const pckt = (new CRFPacket(PacketType.JSON, env.client.id,
          { a: 'foo' }, Date.now())).serialize();

        // fragmentation
        const frag1 =
          pckt.subarray(0, CRFPacket.SYNC_PATTERN.length);
        const frag2 =
          pckt.subarray(CRFPacket.SYNC_PATTERN.length, CRFPacket.HEADER_LENGTH);
        const frag3 =
          pckt.subarray(CRFPacket.HEADER_LENGTH);

        // send packet in three fragments (w/o timeout expiration)
        let fragment = waitForEvent(env.client._tcpSocket, 'data'); // frag #1
        srvSocket.write(frag1);
        expect(await fragment).to.be.deep.equal(frag1);

        fragment = waitForEvent(env.client._tcpSocket, 'data'); // frag #2
        srvSocket.write(frag2);
        expect(await fragment).to.be.deep.equal(frag2);

        const packet = waitForEvent(env.client, 'packet'); // whole packet

        srvSocket.write(frag3);
        expect((await packet).getJSON()).to.be.deep.equal({ a: 'foo' });


        // send packet in two fragments (w/ timeout expiration)
        fragment = waitForEvent(env.client._tcpSocket, 'data'); // frag #1 & #2
        srvSocket.write(Buffer.concat([ frag1, frag2 ]));

        await fragment;
        // expect the first and second fragment in the buffer
        expect(env.client._buffer.buffer())
        .to.be.deep.equal(pckt.subarray(0, CRFPacket.HEADER_LENGTH));

        fragment = waitForEvent(env.client._tcpSocket, 'data', 1,
          CRFSocket.BUFFER_CLEAN_TIMEOUT + 200);
        setTimeout(() => srvSocket.write(frag3), // frag #3 (delayed sending)
          CRFSocket.BUFFER_CLEAN_TIMEOUT + 100);
        expect(await fragment).to.be.deep.equal(frag3);

        // expect only the third fragment in the buffer
        expect(JSON.parse(env.client._buffer.buffer()))
        .to.be.deep.equal({ a: 'foo' });
      }
      catch (err) {
        expect.fail(`Should not fail, but got the error: ${err.message}`);
      }
    });

  it('rejects junk data', async function() {
    env.client = new CRFSocket();

    try {
      const srvConn = waitForEvent(env.server, 'connection');
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;
      const srvSockId = await srvConn;

      const srvSocket = env.server.getSocketById(srvSockId);

      const packet = waitForEvent(env.client, 'packet');
      const error = waitForEvent(env.client, 'error');

      // send junk data + invalid packet
      srvSocket.write(Buffer.alloc(100, '#')); // invalid data
      srvSocket.write(new CRFPacket(PacketType.JSON, env.client.id,
        { a: 'foo' }, Date.now()).serialize()); // valid packet


      expect((await packet).getJSON()).to.be.deep.equal({ a: 'foo' });
      expect((await error).message)
      .to.be.equal('Invalid packet. Packet discarded');
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('closes the socket on writerID mismatch', async function() {
    env.client = new CRFSocket();

    try {
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;

      const error = waitForEvent(env.client, 'error');
      const closure = waitForEvent(env.client, 'close');
      const pckt = new CRFPacket(PacketType.JSON, env.client.id, { a: 'foo' },
        Date.now());

      env.server.prepareReply((new CRFPacket(
        PacketType.JSON,
        env.client.id + 1, // invalidate the writer id
        { /* empty JSON */ },
        Date.now()
      )).serialize());

      env.client.send(pckt);

      await closure;
      expect((await error).message).to.include('writerID mismatch');
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('raises an error when the CRC check fails', async function() {
    env.client = new CRFSocket();

    try {
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;

      const error = waitForEvent(env.client, 'error');
      const pckt = new CRFPacket(PacketType.JSON, env.client.id, { a: 'foo' },
        Date.now());

      const reply = new CRFPacket(PacketType.JSON, env.client.id, { b: 'bar' },
        Date.now());
      reply._length = 0; // invalidate CRC
      env.server.prepareReply(reply.serialize());

      env.client.send(pckt);

      expect((await error).message)
      .to.be.equal('CRC check failed. Packet discarded');
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });

  it('raises an error when receive ill-formed packets', async function() {
    env.client = new CRFSocket();

    try {
      const srvConn = waitForEvent(env.server, 'connection');
      const cliConn = waitForEvent(env.client, 'connected');

      env.client.connect('localhost', env.server?.port);
      await cliConn;
      const srvSockId = await srvConn;

      const srvSocket = env.server.getSocketById(srvSockId);

      const error = waitForEvent(env.client, 'error');

      // send invalid packet
      srvSocket.write(Buffer.alloc(CRFPacket.HEADER_LENGTH + 1, '#'));

      expect((await error).message)
      .to.be.equal('Invalid packet. Packet discarded');
    }
    catch (err) {
      expect.fail(`Should not fail, but got the error: ${err.message}`);
    }
  });
});
