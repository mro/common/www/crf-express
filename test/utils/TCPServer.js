// @ts-check

const
  debug = require('debug')('crf:utils:tcp:server'),
  { makeDeferred } = require('@cern/prom'),
  { EventEmitter } = require("node:events"),
  { isFunction, isInteger, toString } = require('lodash'),
  { createServer } = require('net');

/**
 * @typedef {import('net').Server} Server
 * @typedef {import('net').Socket} Socket
 * @typedef {import('net').AddressInfo} Address
 */


/**
 * @brief TCP Server
 * @emits:
 *  - connection, when a new connection has been established (arg: socketId)
 *  - error, when an error occurs
 */
class TCPServer extends EventEmitter {
  /** @type {Map<number, Socket>} */
  #sockets = new Map();

  /** @type {Server|null} */
  #server = null;

  /** @type {number} */
  #socketId = 0;

  /** @type {Buffer|null} */
  #buffer = null;

  /** @type {boolean} */
  #echo = false;

  /**
   * @param {number} [port]
   * @param {() => any} [cb]
   * @return {Promise<any>}
   */
  async listen(port, cb) {
    if (this.#server?.listening) { return null; }
    const def = makeDeferred();

    const onData = (data) => {
      debug('received data: %s', toString(data));
      if (this.#echo) { this.#buffer = data; }

      if (!this.#buffer) { return; }

      this.send(this.#buffer);
      this.#buffer = null;
    };

    this.#server = createServer();
    this.#server
    .on('error', (err) => {
      this.emit('error', err);
      console.error(`TCP server error: ${err.message}`);
    })
    .on('connection', (socket) => {
      debug('new connection: %s:%d', socket.localAddress, socket.localPort);

      socket
      .on('data', onData)
      .once('close', () => socket.removeListener('data', onData));

      this.#sockets.set(this.#socketId, socket);
      this.emit('connection', this.#socketId++);
    })
    .once('close', () => {
      if (def.isPending) {
        def.reject(new Error('TCP Server is closing'));
      }

      this.#server?.removeAllListeners();
      this.#sockets.forEach((s) => s.destroy());
      this.#sockets.clear();
      this.#server?.unref();
      this.#server = null;
    })
    .listen(port ?? this.port, 'localhost', () => {
      this.#server?.ref();

      this.port = /** @type {Address} */(this.#server?.address()).port;
      debug('listening on: locahost:%d', this.port);

      if (def.isPending) {
        /* eslint-disable callback-return */
        def.resolve(isFunction(cb) ? cb() : true);
      }
    });

    return def.promise;
  }

  /** @param {boolean} enable */
  echo(enable) {
    this.#echo = enable;
  }

  /**
   * @param {number} id
   * @returns {Socket|undefined}
   */
  getSocketById(id) {
    return this.#sockets.get(id);
  }

  /**
   * @param {Buffer} data
   * @param {number} [socketId] - broadcast the data if undefined
   */
  send(data, socketId) {
    if (isInteger(socketId)) {
      // @ts-ignore: socketId checked above
      this.#sockets.get(socketId)?.write(data);
    }
    else {
      this.#sockets.forEach((sckt) => sckt?.write(data));
    }
  }

  /**
   * @brief bufferize data as a reply of the next received request
   *        NOTE: mainly used for testing
   * @param {Buffer} data
   */
  prepareReply(data) {
    if (this.#buffer) { return; }
    this.#buffer = data;
  }

  close() {
    if (!this.#server) { return; }
    debug('closing');

    this.#server.close((err) => {
      if (err) {
        console.error(`Error trying to close the TCP server: ${err.message}`);
      }
    });
  }
}

module.exports = TCPServer;
