// @ts-check
/* eslint-disable max-lines */

const
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { expect } = require('chai'),

  { CRFPacket, PacketType } = require('../src/Packets/CRFPacket'),
  { TCPServer, waitForEvent } = require('./utils'),
  Service = require('../src/Service');


describe('Service', function() {
  const env = {
    host: 'localhost',
    port: 4000
  };

  beforeEach(async function() {
    env.crfServer = new TCPServer();
    await env.crfServer.listen(env.port);
  });

  afterEach(function() {
    env.crfServer.close();
    delete env.crfServer;

    env.crfStream?.close();
    delete env.crfStream;

    env.service?.destroy();
    delete env.service;
  });

  it('can get a JSON command reply', async function() {
    const srvName = 'rpsensor';
    const srvCmd = { id: 0, command: 'getStatus', writerIdCheck: false };

    try {
      env.service = new Service(env.host, env.port, srvName);
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 0, command: 'reply', replyCommand: 'getStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());
      const rep = await env.service.getCmdReply(srvCmd);

      expect(rep.message).to.be.deep.equal({ status: 'OK' });
    }
    catch (err) {
      expect.fail(`Should not fail but got the error: ${err.message}`);
    }
  });

  it('can get a JSON command error', async function() {
    let error = false;
    const srvName = 'rpsensor';
    const srvCmd = { id: 123, command: 'getStatus', writerIdCheck: false };

    try {
      env.service = new Service(env.host, env.port, srvName);
      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 123, command: 'reply', replyCommand: 'error',
          message: 'JSON error message'
        },
        Date.now()
      )).serialize());
      const rep = await env.service.getCmdReply(srvCmd);

      expect(rep).to.be.deep.equal({ status: 'OK' });
    }
    catch (err) {
      error = true;
      expect(err.message).to.be.equal('JSON error message');
    }

    if (!error) {
      expect.fail('Should fail due to the received JSON command error');
    }
  });

  it('allows to send multiple JSON command on a stream', async function() {
    const srvName = 'rpsensor';

    try {
      env.service = new Service(env.host, env.port, srvName);

      env.crfStream = await env.service.getStream({ writerIdCheck: false });

      // first command
      env.crfServer.prepareReply((new CRFPacket( // prepare reply
        PacketType.JSON,
        0,
        { // JSON reply
          command: 'reply', replyCommand: 'getStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());

      let rep = waitForEvent(env.crfStream, 'data');
      env.crfStream.sendCmd({ command: 'getStatus' });

      expect(await rep).to.be.equal(JSON.stringify({
        command: 'reply',
        replyCommand: 'getStatus',
        message: { status: 'OK' }
      }));

      // second command
      env.crfServer.prepareReply((new CRFPacket( // prepare reply
        PacketType.JSON,
        0,
        { // JSON reply
          command: 'reply', replyCommand: 'getStatus',
          message: { parm1: 123, param2: 'test' }
        },
        Date.now()
      )).serialize());

      rep = waitForEvent(env.crfStream, 'data');
      env.crfStream.sendCmd({ command: 'getStatus' });

      expect(await rep).to.be.equal(JSON.stringify({
        command: 'reply',
        replyCommand: 'getStatus',
        message: { parm1: 123, param2: 'test' }
      }));

      env.crfStream.close();
    }
    catch (err) {
      expect.fail(`Should not fail but got the error: ${err.message}`);
    }
  });

  it('allows to receive multiple data on a stream', async function() {
    const srvName = 'rpsensor';

    try {
      env.service = new Service(env.host, env.port, srvName);

      // retrieve the server-side socket id
      const srvSocktetId = waitForEvent(env.crfServer, 'connection');

      env.crfStream = await env.service.getStream({ writerIdCheck: false });
      await env.crfStream.open();

      const srvSocket = env.crfServer.getSocketById(await srvSocktetId);

      // receive serveral frame from CRF-server
      for (let i = 0; i < 5; ++i) {
        const ret = waitForEvent(env.crfStream, 'data');

        const pckt = new CRFPacket(PacketType.FRAME,
          env.crfStream._crfSocket.id, // writerID
          Buffer.from(`Frame ${i}`), Date.now());

        srvSocket.write(pckt.serialize());
        expect(await ret).to.be.deep.equal(pckt.serialize());
      }

      env.crfStream.close();
    }
    catch (err) {
      expect.fail(`Should not fail but got the error: ${err.message}`);
    }
  });

  it('allows to receive errors on a stream', async function() {
    const srvName = 'rpsensor';

    try {
      env.service = new Service(env.host, env.port, srvName);

      env.crfStream = await env.service.getStream();
      await env.crfStream.open();

      // JSON error
      env.crfServer.prepareReply((new CRFPacket( // prepare reply
        PacketType.JSON,
        env.crfStream._crfSocket.id,
        { // JSON reply
          command: 'reply', replyCommand: 'error',
          message: 'JSON error message'
        },
        Date.now()
      )).serialize());

      let err = waitForEvent(env.crfStream, 'data');
      env.crfStream.sendCmd({ command: 'getStatus', cmdIdCheck: false });

      expect(await err).to.be.deep.equal(JSON.stringify({
        command: 'reply',
        replyCommand: 'error',
        message: 'JSON error message'
      }));

      // others generic socket errors (e.g. writerID mismatch)
      env.crfServer.prepareReply((new CRFPacket( // prepare reply
        PacketType.JSON,
        env.crfStream._crfSocket.id + 1, // invalidate the writer id
        { // JSON reply
          command: 'reply', replyCommand: 'getStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());

      err = waitForEvent(env.crfStream, 'error');
      env.crfStream.sendCmd({ command: 'getStatus' });
      expect((await err).message).to.include('writerID mismatch');

      env.crfStream.close();
    }
    catch (err) {
      expect.fail(`Should not fail but got the error: ${err.message}`);
    }
  });

  it('raises an error when unable to establish a stream', async function() {
    let error = false;
    const srvName = 'rpsensor';

    try {
      env.service = new Service(env.host, env.port + 1, srvName); // wrong port

      env.crfStream = await env.service.getStream();
      await env.crfStream.open();
    }
    catch (err) {
      error = true;
      expect(err.message).to.contain('Unable to establish a stream');
    }

    if (!error) {
      expect.fail('Should fail due to incorrect port number');
    }
  });

  it('raises an error on command id mismatch', async function() {
    let error = false;
    const srvName = 'rpsensor';
    const srvCmd = { id: 12345, command: 'getStatus', writerIdCheck: false };

    try {
      env.service = new Service(env.host, env.port, srvName);

      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 54321, // invalidate the command id
          command: 'reply', replyCommand: 'getStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());
      await env.service.getCmdReply(srvCmd);
    }
    catch (err) {
      error = true;
      expect(err.message).to.be.equal('Command id mismatch');
    }

    if (!error) {
      expect.fail('Should fail because the command id does not match');
    }
  });

  it('raises an error upon JSON command reply timeout', async function() {
    let error = false;
    const srvName = 'rpsensor';
    const srvCmd = { id: 1, command: 'getStatus', writerIdCheck: false };

    try {
      env.service = new Service(env.host, env.port, srvName);

      await env.service.getCmdReply(srvCmd, 500); // ms
    }
    catch (err) {
      error = true;
      expect(err.message).to.be.equal('CRF reply timeout');
    }

    if (!error) {
      expect.fail('Should fail by triggering the CRF reply timeout');
    }
  });

  it('raises an error trying to use a different host', async function() {
    let error = false;
    const srvName = 'rpsensor';
    const srvCmd = { id: 0, command: 'getStatus', writerIdCheck: false };

    try {
      env.service = new Service(env.host, env.port, srvName);

      env.crfServer.prepareReply((new CRFPacket(
        PacketType.JSON,
        0,
        { // JSON reply
          id: 0,
          command: 'reply', replyCommand: 'getStatus',
          message: { status: 'OK' }
        },
        Date.now()
      )).serialize());
      const rep = await env.service.getCmdReply(srvCmd);

      expect(rep.message).to.be.deep.equal({ status: 'OK' });

      // change host
      env.service.host = 'XXXX';
      env.service.port = '1234';
      await env.service.getCmdReply(srvCmd);
    }
    catch (err) {
      error = true;
      expect(err.message)
      .to.contain('Attempt to connect to a different CRF endpoint (XXXX:1234)');
    }

    if (!error) {
      expect.fail('Should fail when using different hosts');
    }
  });
});
