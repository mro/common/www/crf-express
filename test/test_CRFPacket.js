// @ts-check

const
  { describe, it } = require('mocha'),
  { expect } = require('chai'),

  { CRFPacket, PacketType } = require("../src/Packets/CRFPacket"),
  { BufferOut } = require('../src/utils/IOBuffer');


describe('CRFPacket', function() {
  const env = {
    json: '{ "a": "foo", "b": [ 1, 2, 3 ] }',
    writerID: 2
  };

  it('can create a CRF data packet', function() {
    const timestamp = Date.now();

    const packet = new CRFPacket(PacketType.JSON,
      env.writerID, env.json, timestamp);

    const header = packet.getHeader();
    expect(header.type).to.be.equal(PacketType.JSON);
    expect(header.writerID).to.be.equal(env.writerID);
    expect(header.length).to.be.equal(env.json.length);
    expect(Number(header.timestamp)).to.be.equal(timestamp);

    // compare the packet using the Buffer type payload as input
    const buf = Buffer.from(env.json);
    const pckt = new CRFPacket(PacketType.JSON, env.writerID, buf, timestamp);
    expect(header).to.be.deep.equal(pckt.getHeader());
    expect(packet.getJSON()).to.be.deep.equal(pckt.getJSON());
    expect(packet.getPayload()).to.be.deep.equal(buf);
  });

  it('can serialize a CRF data packet', function() {
    const timestamp = Date.now();
    const packet = new CRFPacket(PacketType.JSON,
      env.writerID, env.json, BigInt(timestamp));

    const serializedPacket = packet.serialize();
    expect(Buffer.isBuffer(serializedPacket)).to.be.true();

    expect(serializedPacket.length)
    .to.be.equal(CRFPacket.HEADER_LENGTH + env.json.length);


    const buff = new BufferOut(
      Buffer.allocUnsafe(CRFPacket.HEADER_LENGTH + env.json.length));

    buff.write(CRFPacket.SYNC_PATTERN, CRFPacket.SYNC_PATTERN.length);
    buff.writeUInt16(PacketType.JSON);
    buff.writeUInt32(env.json.length);
    buff.writeUInt64(timestamp);
    buff.writeUInt8(env.writerID);

    const crc = CRFPacket.computeCRC(PacketType.JSON, env.json.length,
      timestamp, env.writerID);
    buff.writeUInt8(crc);
    buff.write(env.json, env.json.length);

    expect(buff.buffer).to.be.deep.equal(serializedPacket);
  });

  it('can de-serialize a CRF data packet', function() {
    const payload = Buffer.from('PAYLOAD');
    const timestamp = Date.now();

    const buff = new BufferOut(
      Buffer.allocUnsafe(CRFPacket.HEADER_LENGTH + payload.length));

    buff.write(CRFPacket.SYNC_PATTERN, CRFPacket.SYNC_PATTERN.length);
    buff.writeUInt16(PacketType.FRAME);
    buff.writeUInt32(payload.length);
    buff.writeUInt64(timestamp);
    buff.writeUInt8(env.writerID);

    const crc = CRFPacket.computeCRC(PacketType.FRAME, payload.length,
      timestamp, env.writerID);
    buff.writeUInt8(crc);
    buff.write(payload.toString(), payload.length);

    const deserializedPacket = CRFPacket.deserialize(buff.buffer);

    expect(deserializedPacket?.getHeader()).to.be.deep.equal({
      syncPattern: CRFPacket.SYNC_PATTERN,
      type: PacketType.FRAME,
      length: payload.length,
      timestamp: BigInt(timestamp),
      writerID: env.writerID,
      crc
    });
    expect(deserializedPacket?.getPayload()).to.be.deep.equal(payload);
    expect(deserializedPacket?.getJSON()).to.be.null(); // it's FRAME type
  });

  it('can return JSON message', function() {
    const pckt = new CRFPacket(PacketType.JSON, env.writerID, env.json,
      Date.now());

    expect(pckt.getJSON()).to.be.deep.equal(JSON.parse(env.json));
  });
});
