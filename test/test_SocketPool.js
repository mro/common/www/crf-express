// @ts-check

const { afterEach, beforeEach, describe, it } = require('mocha');
const { expect } = require('chai');
const SocketPool = require("../src/SocketPool");
const { TCPServer, waitForEvent } = require('./utils');


describe("SocketPool", function() {
  const env = {
    host: "localhost",
    port: 4000,
    serviceName: 'rpSensor',
    socketNumber: 2
  };

  beforeEach(async function() {
    env.socketPool = new SocketPool(env.serviceName,
      { socketNumber: env.socketNumber });

    env.server = new TCPServer();
    await env.server.listen();
  });

  afterEach(function() {
    env.socketPool?.destroy();
    delete env.socketPool;

    env.server?.close();
    delete env.server;
  });

  it('Can create a new socket', async function() {
    expect(env.socketPool.serviceName).to.be.equal(env.serviceName);

    expect(env.socketPool._socketUsed).to.be.equal(0);
    expect(env.socketPool._freeSocket.size).to.be.equal(0);

    await env.socketPool?.acquireSocket();

    expect(env.socketPool?._socketUsed).to.be.equal(1);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);
  });

  it('Can release a socket', async function() {
    const socket = await env.socketPool?.acquireSocket();

    expect(env.socketPool?._socketUsed).to.be.equal(1);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    env.socketPool?.releaseSocket(socket);

    expect(env.socketPool?._socketUsed).to.be.equal(0);
    expect(env.socketPool?._freeSocket.size).to.be.equal(1);
  });

  it('Can acquire a free socket', async function() {
    const socket = await env.socketPool?.acquireSocket();
    env.socketPool?.releaseSocket(socket);

    expect(env.socketPool?._socketUsed).to.be.equal(0);
    expect(env.socketPool?._freeSocket.size).to.be.equal(1);

    await env.socketPool?.acquireSocket();

    expect(env.socketPool?._socketUsed).to.be.equal(1);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);
  });

  it('Can defer a socket acquisition', async function() {
    const timeout = 2000; // ms
    this.timeout(timeout + 1000);

    expect(env.socketPool?._socketUsed).to.be.equal(0);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    // reach the maximum number of available sockets
    let lastSocket = null;
    for (let i = 0; i < env.socketNumber; ++i) {
      lastSocket = await env.socketPool?.acquireSocket();
    }

    expect(env.socketPool?._socketUsed).to.be.equal(env.socketNumber);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    try {
      const promisedSocket = env.socketPool?.acquireSocket(timeout);

      env.socketPool?.releaseSocket(lastSocket);

      expect(await promisedSocket).to.be.equal(lastSocket);
      expect(env.socketPool?._freeSocket.size).to.be.equal(0);
    }
    catch (err) {
      expect.fail(`Should not fail but got the error: ${err.message}`);
    }
  });

  it('Can defer a socket acquisition (closing a socket)', async function() {
    const timeout = 2000; // ms
    this.timeout(timeout + 1000);

    expect(env.socketPool?._socketUsed).to.be.equal(0);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    // reach the maximum number of available sockets
    let lastSocket = null;
    for (let i = 0; i < env.socketNumber; ++i) {
      lastSocket = await env.socketPool?.acquireSocket();
    }

    expect(env.socketPool?._socketUsed).to.be.equal(env.socketNumber);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    try {
      const connection = waitForEvent(lastSocket, 'connected');
      lastSocket.connect('localhost', env.server?.port);
      await connection;

      const promisedSocket = env.socketPool?.acquireSocket(timeout);
      expect(env.socketPool?._promSocket.size).to.be.equal(1);

      lastSocket.close();

      await promisedSocket;
      expect(env.socketPool?._promSocket.size).to.be.equal(0);
    }
    catch (err) {
      expect.fail(`Should not fail but got the error: ${err.message}`);
    }
  });

  it('Raises an error upon the socket acquisition timeout', async function() {
    let error = false;
    const timeout = 500; // ms

    expect(env.socketPool?._socketUsed).to.be.equal(0);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    // reach the maximum number of available sockets
    for (let i = 0; i < env.socketNumber; ++i) {
      await env.socketPool?.acquireSocket();
    }

    expect(env.socketPool?._socketUsed).to.be.equal(env.socketNumber);
    expect(env.socketPool?._freeSocket.size).to.be.equal(0);

    const start = Date.now();
    try {
      await env.socketPool?.acquireSocket(timeout);
    }
    catch (err) {
      error = true;
      const end = Date.now();
      expect(err).to.be.an('error');
      expect(err.message).to.be.equal('Unable to acquire a socket (time out)');

      expect(end - start).to.be.approximately(timeout, 5);
    }

    if (!error) {
      expect.fail('The socket should not be acquired');
    }
  });
});
